
_tail:     file format elf32-i386


Disassembly of section .text:

00000000 <tail>:

char buf[512];

void
tail(char* filename)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 38             	sub    $0x38,%esp
  int f, readFile, counter;
  char *newLine, *bufCP;

  if((f = open(filename, 0)) < 0){
   6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
   d:	00 
   e:	8b 45 08             	mov    0x8(%ebp),%eax
  11:	89 04 24             	mov    %eax,(%esp)
  14:	e8 9f 04 00 00       	call   4b8 <open>
  19:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  1c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  20:	79 20                	jns    42 <tail+0x42>
    printf(1, "tail: cannot open %s\n", filename);
  22:	8b 45 08             	mov    0x8(%ebp),%eax
  25:	89 44 24 08          	mov    %eax,0x8(%esp)
  29:	c7 44 24 04 bc 09 00 	movl   $0x9bc,0x4(%esp)
  30:	00 
  31:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  38:	e8 b7 05 00 00       	call   5f4 <printf>
    exit();
  3d:	e8 36 04 00 00       	call   478 <exit>
  }

  counter = 0;
  42:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  while((readFile = read(f, buf, sizeof(buf))) > 0){
  49:	eb 38                	jmp    83 <tail+0x83>
    bufCP = buf;
  4b:	c7 45 f4 60 0a 00 00 	movl   $0xa60,-0xc(%ebp)
    while((newLine = strchr(bufCP, '\n')) != 0){
  52:	eb 13                	jmp    67 <tail+0x67>
      *newLine = 0;
  54:	8b 45 f0             	mov    -0x10(%ebp),%eax
  57:	c6 00 00             	movb   $0x0,(%eax)
      bufCP = newLine+1;
  5a:	8b 45 f0             	mov    -0x10(%ebp),%eax
  5d:	83 c0 01             	add    $0x1,%eax
  60:	89 45 f4             	mov    %eax,-0xc(%ebp)
      counter++;
  63:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }

  counter = 0;
  while((readFile = read(f, buf, sizeof(buf))) > 0){
    bufCP = buf;
    while((newLine = strchr(bufCP, '\n')) != 0){
  67:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
  6e:	00 
  6f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  72:	89 04 24             	mov    %eax,(%esp)
  75:	e8 7c 02 00 00       	call   2f6 <strchr>
  7a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  7d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  81:	75 d1                	jne    54 <tail+0x54>
    printf(1, "tail: cannot open %s\n", filename);
    exit();
  }

  counter = 0;
  while((readFile = read(f, buf, sizeof(buf))) > 0){
  83:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
  8a:	00 
  8b:	c7 44 24 04 60 0a 00 	movl   $0xa60,0x4(%esp)
  92:	00 
  93:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  96:	89 04 24             	mov    %eax,(%esp)
  99:	e8 f2 03 00 00       	call   490 <read>
  9e:	89 45 e8             	mov    %eax,-0x18(%ebp)
  a1:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  a5:	7f a4                	jg     4b <tail+0x4b>
      *newLine = 0;
      bufCP = newLine+1;
      counter++;
    }
  }
  close(f);
  a7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  aa:	89 04 24             	mov    %eax,(%esp)
  ad:	e8 ee 03 00 00       	call   4a0 <close>

  if((f = open(filename, 0)) < 0){
  b2:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  b9:	00 
  ba:	8b 45 08             	mov    0x8(%ebp),%eax
  bd:	89 04 24             	mov    %eax,(%esp)
  c0:	e8 f3 03 00 00       	call   4b8 <open>
  c5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  c8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  cc:	79 20                	jns    ee <tail+0xee>
    printf(1, "tail: cannot open %s\n", filename);
  ce:	8b 45 08             	mov    0x8(%ebp),%eax
  d1:	89 44 24 08          	mov    %eax,0x8(%esp)
  d5:	c7 44 24 04 bc 09 00 	movl   $0x9bc,0x4(%esp)
  dc:	00 
  dd:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  e4:	e8 0b 05 00 00       	call   5f4 <printf>
    exit();
  e9:	e8 8a 03 00 00       	call   478 <exit>
  }
  
  if(counter <= 10)
  ee:	83 7d ec 0a          	cmpl   $0xa,-0x14(%ebp)
  f2:	0f 8f b1 00 00 00    	jg     1a9 <tail+0x1a9>
  {
    while((readFile = read(f, buf, sizeof(buf))) > 0)
  f8:	eb 1b                	jmp    115 <tail+0x115>
      write(1, buf, readFile);
  fa:	8b 45 e8             	mov    -0x18(%ebp),%eax
  fd:	89 44 24 08          	mov    %eax,0x8(%esp)
 101:	c7 44 24 04 60 0a 00 	movl   $0xa60,0x4(%esp)
 108:	00 
 109:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 110:	e8 83 03 00 00       	call   498 <write>
    exit();
  }
  
  if(counter <= 10)
  {
    while((readFile = read(f, buf, sizeof(buf))) > 0)
 115:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
 11c:	00 
 11d:	c7 44 24 04 60 0a 00 	movl   $0xa60,0x4(%esp)
 124:	00 
 125:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 128:	89 04 24             	mov    %eax,(%esp)
 12b:	e8 60 03 00 00       	call   490 <read>
 130:	89 45 e8             	mov    %eax,-0x18(%ebp)
 133:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
 137:	7f c1                	jg     fa <tail+0xfa>
      write(1, buf, readFile);
  }
  
  while((readFile = read(f, buf, sizeof(buf))) > 0){
 139:	eb 6f                	jmp    1aa <tail+0x1aa>
    bufCP = buf;
 13b:	c7 45 f4 60 0a 00 00 	movl   $0xa60,-0xc(%ebp)
    while((newLine = strchr(bufCP, '\n')) != 0){     
 142:	eb 47                	jmp    18b <tail+0x18b>
      counter--;
 144:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
      if(counter < 10)
 148:	83 7d ec 09          	cmpl   $0x9,-0x14(%ebp)
 14c:	7f 2e                	jg     17c <tail+0x17c>
      {
        *newLine = '\n';
 14e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 151:	c6 00 0a             	movb   $0xa,(%eax)
        write(1, bufCP, newLine+1 - bufCP);
 154:	8b 45 f0             	mov    -0x10(%ebp),%eax
 157:	83 c0 01             	add    $0x1,%eax
 15a:	89 c2                	mov    %eax,%edx
 15c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 15f:	89 d1                	mov    %edx,%ecx
 161:	29 c1                	sub    %eax,%ecx
 163:	89 c8                	mov    %ecx,%eax
 165:	89 44 24 08          	mov    %eax,0x8(%esp)
 169:	8b 45 f4             	mov    -0xc(%ebp),%eax
 16c:	89 44 24 04          	mov    %eax,0x4(%esp)
 170:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 177:	e8 1c 03 00 00       	call   498 <write>
      }
      *newLine = 0; 
 17c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 17f:	c6 00 00             	movb   $0x0,(%eax)
      bufCP = newLine+1;
 182:	8b 45 f0             	mov    -0x10(%ebp),%eax
 185:	83 c0 01             	add    $0x1,%eax
 188:	89 45 f4             	mov    %eax,-0xc(%ebp)
      write(1, buf, readFile);
  }
  
  while((readFile = read(f, buf, sizeof(buf))) > 0){
    bufCP = buf;
    while((newLine = strchr(bufCP, '\n')) != 0){     
 18b:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
 192:	00 
 193:	8b 45 f4             	mov    -0xc(%ebp),%eax
 196:	89 04 24             	mov    %eax,(%esp)
 199:	e8 58 01 00 00       	call   2f6 <strchr>
 19e:	89 45 f0             	mov    %eax,-0x10(%ebp)
 1a1:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1a5:	75 9d                	jne    144 <tail+0x144>
 1a7:	eb 01                	jmp    1aa <tail+0x1aa>
  {
    while((readFile = read(f, buf, sizeof(buf))) > 0)
      write(1, buf, readFile);
  }
  
  while((readFile = read(f, buf, sizeof(buf))) > 0){
 1a9:	90                   	nop
 1aa:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
 1b1:	00 
 1b2:	c7 44 24 04 60 0a 00 	movl   $0xa60,0x4(%esp)
 1b9:	00 
 1ba:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1bd:	89 04 24             	mov    %eax,(%esp)
 1c0:	e8 cb 02 00 00       	call   490 <read>
 1c5:	89 45 e8             	mov    %eax,-0x18(%ebp)
 1c8:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
 1cc:	0f 8f 69 ff ff ff    	jg     13b <tail+0x13b>
      }
      *newLine = 0; 
      bufCP = newLine+1;
    }
  }
}
 1d2:	c9                   	leave  
 1d3:	c3                   	ret    

000001d4 <main>:

int
main(int argc, char *argv[])
{ 
 1d4:	55                   	push   %ebp
 1d5:	89 e5                	mov    %esp,%ebp
 1d7:	83 e4 f0             	and    $0xfffffff0,%esp
 1da:	83 ec 10             	sub    $0x10,%esp
  if(argc != 2){
 1dd:	83 7d 08 02          	cmpl   $0x2,0x8(%ebp)
 1e1:	74 19                	je     1fc <main+0x28>
    printf(1, "Error: please input the correct amount of arguments\n");
 1e3:	c7 44 24 04 d4 09 00 	movl   $0x9d4,0x4(%esp)
 1ea:	00 
 1eb:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 1f2:	e8 fd 03 00 00       	call   5f4 <printf>
    exit();
 1f7:	e8 7c 02 00 00       	call   478 <exit>
  }
  
  tail(argv[1]);
 1fc:	8b 45 0c             	mov    0xc(%ebp),%eax
 1ff:	83 c0 04             	add    $0x4,%eax
 202:	8b 00                	mov    (%eax),%eax
 204:	89 04 24             	mov    %eax,(%esp)
 207:	e8 f4 fd ff ff       	call   0 <tail>
  exit();
 20c:	e8 67 02 00 00       	call   478 <exit>
 211:	90                   	nop
 212:	90                   	nop
 213:	90                   	nop

00000214 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 214:	55                   	push   %ebp
 215:	89 e5                	mov    %esp,%ebp
 217:	57                   	push   %edi
 218:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 219:	8b 4d 08             	mov    0x8(%ebp),%ecx
 21c:	8b 55 10             	mov    0x10(%ebp),%edx
 21f:	8b 45 0c             	mov    0xc(%ebp),%eax
 222:	89 cb                	mov    %ecx,%ebx
 224:	89 df                	mov    %ebx,%edi
 226:	89 d1                	mov    %edx,%ecx
 228:	fc                   	cld    
 229:	f3 aa                	rep stos %al,%es:(%edi)
 22b:	89 ca                	mov    %ecx,%edx
 22d:	89 fb                	mov    %edi,%ebx
 22f:	89 5d 08             	mov    %ebx,0x8(%ebp)
 232:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 235:	5b                   	pop    %ebx
 236:	5f                   	pop    %edi
 237:	5d                   	pop    %ebp
 238:	c3                   	ret    

00000239 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 239:	55                   	push   %ebp
 23a:	89 e5                	mov    %esp,%ebp
 23c:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 23f:	8b 45 08             	mov    0x8(%ebp),%eax
 242:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 245:	8b 45 0c             	mov    0xc(%ebp),%eax
 248:	0f b6 10             	movzbl (%eax),%edx
 24b:	8b 45 08             	mov    0x8(%ebp),%eax
 24e:	88 10                	mov    %dl,(%eax)
 250:	8b 45 08             	mov    0x8(%ebp),%eax
 253:	0f b6 00             	movzbl (%eax),%eax
 256:	84 c0                	test   %al,%al
 258:	0f 95 c0             	setne  %al
 25b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 25f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 263:	84 c0                	test   %al,%al
 265:	75 de                	jne    245 <strcpy+0xc>
    ;
  return os;
 267:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 26a:	c9                   	leave  
 26b:	c3                   	ret    

0000026c <strcmp>:

int
strcmp(const char *p, const char *q)
{
 26c:	55                   	push   %ebp
 26d:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 26f:	eb 08                	jmp    279 <strcmp+0xd>
    p++, q++;
 271:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 275:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 279:	8b 45 08             	mov    0x8(%ebp),%eax
 27c:	0f b6 00             	movzbl (%eax),%eax
 27f:	84 c0                	test   %al,%al
 281:	74 10                	je     293 <strcmp+0x27>
 283:	8b 45 08             	mov    0x8(%ebp),%eax
 286:	0f b6 10             	movzbl (%eax),%edx
 289:	8b 45 0c             	mov    0xc(%ebp),%eax
 28c:	0f b6 00             	movzbl (%eax),%eax
 28f:	38 c2                	cmp    %al,%dl
 291:	74 de                	je     271 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 293:	8b 45 08             	mov    0x8(%ebp),%eax
 296:	0f b6 00             	movzbl (%eax),%eax
 299:	0f b6 d0             	movzbl %al,%edx
 29c:	8b 45 0c             	mov    0xc(%ebp),%eax
 29f:	0f b6 00             	movzbl (%eax),%eax
 2a2:	0f b6 c0             	movzbl %al,%eax
 2a5:	89 d1                	mov    %edx,%ecx
 2a7:	29 c1                	sub    %eax,%ecx
 2a9:	89 c8                	mov    %ecx,%eax
}
 2ab:	5d                   	pop    %ebp
 2ac:	c3                   	ret    

000002ad <strlen>:

uint
strlen(char *s)
{
 2ad:	55                   	push   %ebp
 2ae:	89 e5                	mov    %esp,%ebp
 2b0:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 2b3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 2ba:	eb 04                	jmp    2c0 <strlen+0x13>
 2bc:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 2c0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2c3:	03 45 08             	add    0x8(%ebp),%eax
 2c6:	0f b6 00             	movzbl (%eax),%eax
 2c9:	84 c0                	test   %al,%al
 2cb:	75 ef                	jne    2bc <strlen+0xf>
    ;
  return n;
 2cd:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2d0:	c9                   	leave  
 2d1:	c3                   	ret    

000002d2 <memset>:

void*
memset(void *dst, int c, uint n)
{
 2d2:	55                   	push   %ebp
 2d3:	89 e5                	mov    %esp,%ebp
 2d5:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 2d8:	8b 45 10             	mov    0x10(%ebp),%eax
 2db:	89 44 24 08          	mov    %eax,0x8(%esp)
 2df:	8b 45 0c             	mov    0xc(%ebp),%eax
 2e2:	89 44 24 04          	mov    %eax,0x4(%esp)
 2e6:	8b 45 08             	mov    0x8(%ebp),%eax
 2e9:	89 04 24             	mov    %eax,(%esp)
 2ec:	e8 23 ff ff ff       	call   214 <stosb>
  return dst;
 2f1:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2f4:	c9                   	leave  
 2f5:	c3                   	ret    

000002f6 <strchr>:

char*
strchr(const char *s, char c)
{
 2f6:	55                   	push   %ebp
 2f7:	89 e5                	mov    %esp,%ebp
 2f9:	83 ec 04             	sub    $0x4,%esp
 2fc:	8b 45 0c             	mov    0xc(%ebp),%eax
 2ff:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 302:	eb 14                	jmp    318 <strchr+0x22>
    if(*s == c)
 304:	8b 45 08             	mov    0x8(%ebp),%eax
 307:	0f b6 00             	movzbl (%eax),%eax
 30a:	3a 45 fc             	cmp    -0x4(%ebp),%al
 30d:	75 05                	jne    314 <strchr+0x1e>
      return (char*)s;
 30f:	8b 45 08             	mov    0x8(%ebp),%eax
 312:	eb 13                	jmp    327 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 314:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 318:	8b 45 08             	mov    0x8(%ebp),%eax
 31b:	0f b6 00             	movzbl (%eax),%eax
 31e:	84 c0                	test   %al,%al
 320:	75 e2                	jne    304 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 322:	b8 00 00 00 00       	mov    $0x0,%eax
}
 327:	c9                   	leave  
 328:	c3                   	ret    

00000329 <gets>:

char*
gets(char *buf, int max)
{
 329:	55                   	push   %ebp
 32a:	89 e5                	mov    %esp,%ebp
 32c:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 32f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 336:	eb 44                	jmp    37c <gets+0x53>
    cc = read(0, &c, 1);
 338:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 33f:	00 
 340:	8d 45 ef             	lea    -0x11(%ebp),%eax
 343:	89 44 24 04          	mov    %eax,0x4(%esp)
 347:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 34e:	e8 3d 01 00 00       	call   490 <read>
 353:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 356:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 35a:	7e 2d                	jle    389 <gets+0x60>
      break;
    buf[i++] = c;
 35c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 35f:	03 45 08             	add    0x8(%ebp),%eax
 362:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 366:	88 10                	mov    %dl,(%eax)
 368:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 36c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 370:	3c 0a                	cmp    $0xa,%al
 372:	74 16                	je     38a <gets+0x61>
 374:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 378:	3c 0d                	cmp    $0xd,%al
 37a:	74 0e                	je     38a <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 37c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 37f:	83 c0 01             	add    $0x1,%eax
 382:	3b 45 0c             	cmp    0xc(%ebp),%eax
 385:	7c b1                	jl     338 <gets+0xf>
 387:	eb 01                	jmp    38a <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 389:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 38a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 38d:	03 45 08             	add    0x8(%ebp),%eax
 390:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 393:	8b 45 08             	mov    0x8(%ebp),%eax
}
 396:	c9                   	leave  
 397:	c3                   	ret    

00000398 <stat>:

int
stat(char *n, struct stat *st)
{
 398:	55                   	push   %ebp
 399:	89 e5                	mov    %esp,%ebp
 39b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 39e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 3a5:	00 
 3a6:	8b 45 08             	mov    0x8(%ebp),%eax
 3a9:	89 04 24             	mov    %eax,(%esp)
 3ac:	e8 07 01 00 00       	call   4b8 <open>
 3b1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 3b4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 3b8:	79 07                	jns    3c1 <stat+0x29>
    return -1;
 3ba:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 3bf:	eb 23                	jmp    3e4 <stat+0x4c>
  r = fstat(fd, st);
 3c1:	8b 45 0c             	mov    0xc(%ebp),%eax
 3c4:	89 44 24 04          	mov    %eax,0x4(%esp)
 3c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3cb:	89 04 24             	mov    %eax,(%esp)
 3ce:	e8 fd 00 00 00       	call   4d0 <fstat>
 3d3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 3d6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 3d9:	89 04 24             	mov    %eax,(%esp)
 3dc:	e8 bf 00 00 00       	call   4a0 <close>
  return r;
 3e1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 3e4:	c9                   	leave  
 3e5:	c3                   	ret    

000003e6 <atoi>:

int
atoi(const char *s)
{
 3e6:	55                   	push   %ebp
 3e7:	89 e5                	mov    %esp,%ebp
 3e9:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 3ec:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 3f3:	eb 24                	jmp    419 <atoi+0x33>
    n = n*10 + *s++ - '0';
 3f5:	8b 55 fc             	mov    -0x4(%ebp),%edx
 3f8:	89 d0                	mov    %edx,%eax
 3fa:	c1 e0 02             	shl    $0x2,%eax
 3fd:	01 d0                	add    %edx,%eax
 3ff:	01 c0                	add    %eax,%eax
 401:	89 c2                	mov    %eax,%edx
 403:	8b 45 08             	mov    0x8(%ebp),%eax
 406:	0f b6 00             	movzbl (%eax),%eax
 409:	0f be c0             	movsbl %al,%eax
 40c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 40f:	83 e8 30             	sub    $0x30,%eax
 412:	89 45 fc             	mov    %eax,-0x4(%ebp)
 415:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 419:	8b 45 08             	mov    0x8(%ebp),%eax
 41c:	0f b6 00             	movzbl (%eax),%eax
 41f:	3c 2f                	cmp    $0x2f,%al
 421:	7e 0a                	jle    42d <atoi+0x47>
 423:	8b 45 08             	mov    0x8(%ebp),%eax
 426:	0f b6 00             	movzbl (%eax),%eax
 429:	3c 39                	cmp    $0x39,%al
 42b:	7e c8                	jle    3f5 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 42d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 430:	c9                   	leave  
 431:	c3                   	ret    

00000432 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 432:	55                   	push   %ebp
 433:	89 e5                	mov    %esp,%ebp
 435:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 438:	8b 45 08             	mov    0x8(%ebp),%eax
 43b:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 43e:	8b 45 0c             	mov    0xc(%ebp),%eax
 441:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 444:	eb 13                	jmp    459 <memmove+0x27>
    *dst++ = *src++;
 446:	8b 45 fc             	mov    -0x4(%ebp),%eax
 449:	0f b6 10             	movzbl (%eax),%edx
 44c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 44f:	88 10                	mov    %dl,(%eax)
 451:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 455:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 459:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 45d:	0f 9f c0             	setg   %al
 460:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 464:	84 c0                	test   %al,%al
 466:	75 de                	jne    446 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 468:	8b 45 08             	mov    0x8(%ebp),%eax
}
 46b:	c9                   	leave  
 46c:	c3                   	ret    
 46d:	90                   	nop
 46e:	90                   	nop
 46f:	90                   	nop

00000470 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 470:	b8 01 00 00 00       	mov    $0x1,%eax
 475:	cd 40                	int    $0x40
 477:	c3                   	ret    

00000478 <exit>:
SYSCALL(exit)
 478:	b8 02 00 00 00       	mov    $0x2,%eax
 47d:	cd 40                	int    $0x40
 47f:	c3                   	ret    

00000480 <wait>:
SYSCALL(wait)
 480:	b8 03 00 00 00       	mov    $0x3,%eax
 485:	cd 40                	int    $0x40
 487:	c3                   	ret    

00000488 <pipe>:
SYSCALL(pipe)
 488:	b8 04 00 00 00       	mov    $0x4,%eax
 48d:	cd 40                	int    $0x40
 48f:	c3                   	ret    

00000490 <read>:
SYSCALL(read)
 490:	b8 05 00 00 00       	mov    $0x5,%eax
 495:	cd 40                	int    $0x40
 497:	c3                   	ret    

00000498 <write>:
SYSCALL(write)
 498:	b8 10 00 00 00       	mov    $0x10,%eax
 49d:	cd 40                	int    $0x40
 49f:	c3                   	ret    

000004a0 <close>:
SYSCALL(close)
 4a0:	b8 15 00 00 00       	mov    $0x15,%eax
 4a5:	cd 40                	int    $0x40
 4a7:	c3                   	ret    

000004a8 <kill>:
SYSCALL(kill)
 4a8:	b8 06 00 00 00       	mov    $0x6,%eax
 4ad:	cd 40                	int    $0x40
 4af:	c3                   	ret    

000004b0 <exec>:
SYSCALL(exec)
 4b0:	b8 07 00 00 00       	mov    $0x7,%eax
 4b5:	cd 40                	int    $0x40
 4b7:	c3                   	ret    

000004b8 <open>:
SYSCALL(open)
 4b8:	b8 0f 00 00 00       	mov    $0xf,%eax
 4bd:	cd 40                	int    $0x40
 4bf:	c3                   	ret    

000004c0 <mknod>:
SYSCALL(mknod)
 4c0:	b8 11 00 00 00       	mov    $0x11,%eax
 4c5:	cd 40                	int    $0x40
 4c7:	c3                   	ret    

000004c8 <unlink>:
SYSCALL(unlink)
 4c8:	b8 12 00 00 00       	mov    $0x12,%eax
 4cd:	cd 40                	int    $0x40
 4cf:	c3                   	ret    

000004d0 <fstat>:
SYSCALL(fstat)
 4d0:	b8 08 00 00 00       	mov    $0x8,%eax
 4d5:	cd 40                	int    $0x40
 4d7:	c3                   	ret    

000004d8 <link>:
SYSCALL(link)
 4d8:	b8 13 00 00 00       	mov    $0x13,%eax
 4dd:	cd 40                	int    $0x40
 4df:	c3                   	ret    

000004e0 <mkdir>:
SYSCALL(mkdir)
 4e0:	b8 14 00 00 00       	mov    $0x14,%eax
 4e5:	cd 40                	int    $0x40
 4e7:	c3                   	ret    

000004e8 <chdir>:
SYSCALL(chdir)
 4e8:	b8 09 00 00 00       	mov    $0x9,%eax
 4ed:	cd 40                	int    $0x40
 4ef:	c3                   	ret    

000004f0 <dup>:
SYSCALL(dup)
 4f0:	b8 0a 00 00 00       	mov    $0xa,%eax
 4f5:	cd 40                	int    $0x40
 4f7:	c3                   	ret    

000004f8 <getpid>:
SYSCALL(getpid)
 4f8:	b8 0b 00 00 00       	mov    $0xb,%eax
 4fd:	cd 40                	int    $0x40
 4ff:	c3                   	ret    

00000500 <sbrk>:
SYSCALL(sbrk)
 500:	b8 0c 00 00 00       	mov    $0xc,%eax
 505:	cd 40                	int    $0x40
 507:	c3                   	ret    

00000508 <sleep>:
SYSCALL(sleep)
 508:	b8 0d 00 00 00       	mov    $0xd,%eax
 50d:	cd 40                	int    $0x40
 50f:	c3                   	ret    

00000510 <uptime>:
SYSCALL(uptime)
 510:	b8 0e 00 00 00       	mov    $0xe,%eax
 515:	cd 40                	int    $0x40
 517:	c3                   	ret    

00000518 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 518:	55                   	push   %ebp
 519:	89 e5                	mov    %esp,%ebp
 51b:	83 ec 28             	sub    $0x28,%esp
 51e:	8b 45 0c             	mov    0xc(%ebp),%eax
 521:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 524:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 52b:	00 
 52c:	8d 45 f4             	lea    -0xc(%ebp),%eax
 52f:	89 44 24 04          	mov    %eax,0x4(%esp)
 533:	8b 45 08             	mov    0x8(%ebp),%eax
 536:	89 04 24             	mov    %eax,(%esp)
 539:	e8 5a ff ff ff       	call   498 <write>
}
 53e:	c9                   	leave  
 53f:	c3                   	ret    

00000540 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 540:	55                   	push   %ebp
 541:	89 e5                	mov    %esp,%ebp
 543:	53                   	push   %ebx
 544:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 547:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 54e:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 552:	74 17                	je     56b <printint+0x2b>
 554:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 558:	79 11                	jns    56b <printint+0x2b>
    neg = 1;
 55a:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 561:	8b 45 0c             	mov    0xc(%ebp),%eax
 564:	f7 d8                	neg    %eax
 566:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 569:	eb 06                	jmp    571 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 56b:	8b 45 0c             	mov    0xc(%ebp),%eax
 56e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 571:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 578:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 57b:	8b 5d 10             	mov    0x10(%ebp),%ebx
 57e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 581:	ba 00 00 00 00       	mov    $0x0,%edx
 586:	f7 f3                	div    %ebx
 588:	89 d0                	mov    %edx,%eax
 58a:	0f b6 80 10 0a 00 00 	movzbl 0xa10(%eax),%eax
 591:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 595:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 599:	8b 45 10             	mov    0x10(%ebp),%eax
 59c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 59f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5a2:	ba 00 00 00 00       	mov    $0x0,%edx
 5a7:	f7 75 d4             	divl   -0x2c(%ebp)
 5aa:	89 45 f4             	mov    %eax,-0xc(%ebp)
 5ad:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 5b1:	75 c5                	jne    578 <printint+0x38>
  if(neg)
 5b3:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 5b7:	74 2a                	je     5e3 <printint+0xa3>
    buf[i++] = '-';
 5b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5bc:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 5c1:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 5c5:	eb 1d                	jmp    5e4 <printint+0xa4>
    putc(fd, buf[i]);
 5c7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5ca:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 5cf:	0f be c0             	movsbl %al,%eax
 5d2:	89 44 24 04          	mov    %eax,0x4(%esp)
 5d6:	8b 45 08             	mov    0x8(%ebp),%eax
 5d9:	89 04 24             	mov    %eax,(%esp)
 5dc:	e8 37 ff ff ff       	call   518 <putc>
 5e1:	eb 01                	jmp    5e4 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 5e3:	90                   	nop
 5e4:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 5e8:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 5ec:	79 d9                	jns    5c7 <printint+0x87>
    putc(fd, buf[i]);
}
 5ee:	83 c4 44             	add    $0x44,%esp
 5f1:	5b                   	pop    %ebx
 5f2:	5d                   	pop    %ebp
 5f3:	c3                   	ret    

000005f4 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 5f4:	55                   	push   %ebp
 5f5:	89 e5                	mov    %esp,%ebp
 5f7:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 5fa:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 601:	8d 45 0c             	lea    0xc(%ebp),%eax
 604:	83 c0 04             	add    $0x4,%eax
 607:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 60a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 611:	e9 7e 01 00 00       	jmp    794 <printf+0x1a0>
    c = fmt[i] & 0xff;
 616:	8b 55 0c             	mov    0xc(%ebp),%edx
 619:	8b 45 ec             	mov    -0x14(%ebp),%eax
 61c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 61f:	0f b6 00             	movzbl (%eax),%eax
 622:	0f be c0             	movsbl %al,%eax
 625:	25 ff 00 00 00       	and    $0xff,%eax
 62a:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 62d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 631:	75 2c                	jne    65f <printf+0x6b>
      if(c == '%'){
 633:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 637:	75 0c                	jne    645 <printf+0x51>
        state = '%';
 639:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 640:	e9 4b 01 00 00       	jmp    790 <printf+0x19c>
      } else {
        putc(fd, c);
 645:	8b 45 e8             	mov    -0x18(%ebp),%eax
 648:	0f be c0             	movsbl %al,%eax
 64b:	89 44 24 04          	mov    %eax,0x4(%esp)
 64f:	8b 45 08             	mov    0x8(%ebp),%eax
 652:	89 04 24             	mov    %eax,(%esp)
 655:	e8 be fe ff ff       	call   518 <putc>
 65a:	e9 31 01 00 00       	jmp    790 <printf+0x19c>
      }
    } else if(state == '%'){
 65f:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 663:	0f 85 27 01 00 00    	jne    790 <printf+0x19c>
      if(c == 'd'){
 669:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 66d:	75 2d                	jne    69c <printf+0xa8>
        printint(fd, *ap, 10, 1);
 66f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 672:	8b 00                	mov    (%eax),%eax
 674:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 67b:	00 
 67c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 683:	00 
 684:	89 44 24 04          	mov    %eax,0x4(%esp)
 688:	8b 45 08             	mov    0x8(%ebp),%eax
 68b:	89 04 24             	mov    %eax,(%esp)
 68e:	e8 ad fe ff ff       	call   540 <printint>
        ap++;
 693:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 697:	e9 ed 00 00 00       	jmp    789 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 69c:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 6a0:	74 06                	je     6a8 <printf+0xb4>
 6a2:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 6a6:	75 2d                	jne    6d5 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 6a8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6ab:	8b 00                	mov    (%eax),%eax
 6ad:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 6b4:	00 
 6b5:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 6bc:	00 
 6bd:	89 44 24 04          	mov    %eax,0x4(%esp)
 6c1:	8b 45 08             	mov    0x8(%ebp),%eax
 6c4:	89 04 24             	mov    %eax,(%esp)
 6c7:	e8 74 fe ff ff       	call   540 <printint>
        ap++;
 6cc:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 6d0:	e9 b4 00 00 00       	jmp    789 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 6d5:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 6d9:	75 46                	jne    721 <printf+0x12d>
        s = (char*)*ap;
 6db:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6de:	8b 00                	mov    (%eax),%eax
 6e0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 6e3:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 6e7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 6eb:	75 27                	jne    714 <printf+0x120>
          s = "(null)";
 6ed:	c7 45 e4 09 0a 00 00 	movl   $0xa09,-0x1c(%ebp)
        while(*s != 0){
 6f4:	eb 1f                	jmp    715 <printf+0x121>
          putc(fd, *s);
 6f6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6f9:	0f b6 00             	movzbl (%eax),%eax
 6fc:	0f be c0             	movsbl %al,%eax
 6ff:	89 44 24 04          	mov    %eax,0x4(%esp)
 703:	8b 45 08             	mov    0x8(%ebp),%eax
 706:	89 04 24             	mov    %eax,(%esp)
 709:	e8 0a fe ff ff       	call   518 <putc>
          s++;
 70e:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 712:	eb 01                	jmp    715 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 714:	90                   	nop
 715:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 718:	0f b6 00             	movzbl (%eax),%eax
 71b:	84 c0                	test   %al,%al
 71d:	75 d7                	jne    6f6 <printf+0x102>
 71f:	eb 68                	jmp    789 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 721:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 725:	75 1d                	jne    744 <printf+0x150>
        putc(fd, *ap);
 727:	8b 45 f4             	mov    -0xc(%ebp),%eax
 72a:	8b 00                	mov    (%eax),%eax
 72c:	0f be c0             	movsbl %al,%eax
 72f:	89 44 24 04          	mov    %eax,0x4(%esp)
 733:	8b 45 08             	mov    0x8(%ebp),%eax
 736:	89 04 24             	mov    %eax,(%esp)
 739:	e8 da fd ff ff       	call   518 <putc>
        ap++;
 73e:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 742:	eb 45                	jmp    789 <printf+0x195>
      } else if(c == '%'){
 744:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 748:	75 17                	jne    761 <printf+0x16d>
        putc(fd, c);
 74a:	8b 45 e8             	mov    -0x18(%ebp),%eax
 74d:	0f be c0             	movsbl %al,%eax
 750:	89 44 24 04          	mov    %eax,0x4(%esp)
 754:	8b 45 08             	mov    0x8(%ebp),%eax
 757:	89 04 24             	mov    %eax,(%esp)
 75a:	e8 b9 fd ff ff       	call   518 <putc>
 75f:	eb 28                	jmp    789 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 761:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 768:	00 
 769:	8b 45 08             	mov    0x8(%ebp),%eax
 76c:	89 04 24             	mov    %eax,(%esp)
 76f:	e8 a4 fd ff ff       	call   518 <putc>
        putc(fd, c);
 774:	8b 45 e8             	mov    -0x18(%ebp),%eax
 777:	0f be c0             	movsbl %al,%eax
 77a:	89 44 24 04          	mov    %eax,0x4(%esp)
 77e:	8b 45 08             	mov    0x8(%ebp),%eax
 781:	89 04 24             	mov    %eax,(%esp)
 784:	e8 8f fd ff ff       	call   518 <putc>
      }
      state = 0;
 789:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 790:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 794:	8b 55 0c             	mov    0xc(%ebp),%edx
 797:	8b 45 ec             	mov    -0x14(%ebp),%eax
 79a:	8d 04 02             	lea    (%edx,%eax,1),%eax
 79d:	0f b6 00             	movzbl (%eax),%eax
 7a0:	84 c0                	test   %al,%al
 7a2:	0f 85 6e fe ff ff    	jne    616 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 7a8:	c9                   	leave  
 7a9:	c3                   	ret    
 7aa:	90                   	nop
 7ab:	90                   	nop

000007ac <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 7ac:	55                   	push   %ebp
 7ad:	89 e5                	mov    %esp,%ebp
 7af:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 7b2:	8b 45 08             	mov    0x8(%ebp),%eax
 7b5:	83 e8 08             	sub    $0x8,%eax
 7b8:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7bb:	a1 48 0a 00 00       	mov    0xa48,%eax
 7c0:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7c3:	eb 24                	jmp    7e9 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 7c5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7c8:	8b 00                	mov    (%eax),%eax
 7ca:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7cd:	77 12                	ja     7e1 <free+0x35>
 7cf:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7d2:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7d5:	77 24                	ja     7fb <free+0x4f>
 7d7:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7da:	8b 00                	mov    (%eax),%eax
 7dc:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7df:	77 1a                	ja     7fb <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7e1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7e4:	8b 00                	mov    (%eax),%eax
 7e6:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7e9:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7ec:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7ef:	76 d4                	jbe    7c5 <free+0x19>
 7f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7f4:	8b 00                	mov    (%eax),%eax
 7f6:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7f9:	76 ca                	jbe    7c5 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 7fb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7fe:	8b 40 04             	mov    0x4(%eax),%eax
 801:	c1 e0 03             	shl    $0x3,%eax
 804:	89 c2                	mov    %eax,%edx
 806:	03 55 f8             	add    -0x8(%ebp),%edx
 809:	8b 45 fc             	mov    -0x4(%ebp),%eax
 80c:	8b 00                	mov    (%eax),%eax
 80e:	39 c2                	cmp    %eax,%edx
 810:	75 24                	jne    836 <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 812:	8b 45 f8             	mov    -0x8(%ebp),%eax
 815:	8b 50 04             	mov    0x4(%eax),%edx
 818:	8b 45 fc             	mov    -0x4(%ebp),%eax
 81b:	8b 00                	mov    (%eax),%eax
 81d:	8b 40 04             	mov    0x4(%eax),%eax
 820:	01 c2                	add    %eax,%edx
 822:	8b 45 f8             	mov    -0x8(%ebp),%eax
 825:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 828:	8b 45 fc             	mov    -0x4(%ebp),%eax
 82b:	8b 00                	mov    (%eax),%eax
 82d:	8b 10                	mov    (%eax),%edx
 82f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 832:	89 10                	mov    %edx,(%eax)
 834:	eb 0a                	jmp    840 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 836:	8b 45 fc             	mov    -0x4(%ebp),%eax
 839:	8b 10                	mov    (%eax),%edx
 83b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 83e:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 840:	8b 45 fc             	mov    -0x4(%ebp),%eax
 843:	8b 40 04             	mov    0x4(%eax),%eax
 846:	c1 e0 03             	shl    $0x3,%eax
 849:	03 45 fc             	add    -0x4(%ebp),%eax
 84c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 84f:	75 20                	jne    871 <free+0xc5>
    p->s.size += bp->s.size;
 851:	8b 45 fc             	mov    -0x4(%ebp),%eax
 854:	8b 50 04             	mov    0x4(%eax),%edx
 857:	8b 45 f8             	mov    -0x8(%ebp),%eax
 85a:	8b 40 04             	mov    0x4(%eax),%eax
 85d:	01 c2                	add    %eax,%edx
 85f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 862:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 865:	8b 45 f8             	mov    -0x8(%ebp),%eax
 868:	8b 10                	mov    (%eax),%edx
 86a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 86d:	89 10                	mov    %edx,(%eax)
 86f:	eb 08                	jmp    879 <free+0xcd>
  } else
    p->s.ptr = bp;
 871:	8b 45 fc             	mov    -0x4(%ebp),%eax
 874:	8b 55 f8             	mov    -0x8(%ebp),%edx
 877:	89 10                	mov    %edx,(%eax)
  freep = p;
 879:	8b 45 fc             	mov    -0x4(%ebp),%eax
 87c:	a3 48 0a 00 00       	mov    %eax,0xa48
}
 881:	c9                   	leave  
 882:	c3                   	ret    

00000883 <morecore>:

static Header*
morecore(uint nu)
{
 883:	55                   	push   %ebp
 884:	89 e5                	mov    %esp,%ebp
 886:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 889:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 890:	77 07                	ja     899 <morecore+0x16>
    nu = 4096;
 892:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 899:	8b 45 08             	mov    0x8(%ebp),%eax
 89c:	c1 e0 03             	shl    $0x3,%eax
 89f:	89 04 24             	mov    %eax,(%esp)
 8a2:	e8 59 fc ff ff       	call   500 <sbrk>
 8a7:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 8aa:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 8ae:	75 07                	jne    8b7 <morecore+0x34>
    return 0;
 8b0:	b8 00 00 00 00       	mov    $0x0,%eax
 8b5:	eb 22                	jmp    8d9 <morecore+0x56>
  hp = (Header*)p;
 8b7:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8ba:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 8bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8c0:	8b 55 08             	mov    0x8(%ebp),%edx
 8c3:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 8c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8c9:	83 c0 08             	add    $0x8,%eax
 8cc:	89 04 24             	mov    %eax,(%esp)
 8cf:	e8 d8 fe ff ff       	call   7ac <free>
  return freep;
 8d4:	a1 48 0a 00 00       	mov    0xa48,%eax
}
 8d9:	c9                   	leave  
 8da:	c3                   	ret    

000008db <malloc>:

void*
malloc(uint nbytes)
{
 8db:	55                   	push   %ebp
 8dc:	89 e5                	mov    %esp,%ebp
 8de:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 8e1:	8b 45 08             	mov    0x8(%ebp),%eax
 8e4:	83 c0 07             	add    $0x7,%eax
 8e7:	c1 e8 03             	shr    $0x3,%eax
 8ea:	83 c0 01             	add    $0x1,%eax
 8ed:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 8f0:	a1 48 0a 00 00       	mov    0xa48,%eax
 8f5:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8f8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 8fc:	75 23                	jne    921 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 8fe:	c7 45 f0 40 0a 00 00 	movl   $0xa40,-0x10(%ebp)
 905:	8b 45 f0             	mov    -0x10(%ebp),%eax
 908:	a3 48 0a 00 00       	mov    %eax,0xa48
 90d:	a1 48 0a 00 00       	mov    0xa48,%eax
 912:	a3 40 0a 00 00       	mov    %eax,0xa40
    base.s.size = 0;
 917:	c7 05 44 0a 00 00 00 	movl   $0x0,0xa44
 91e:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 921:	8b 45 f0             	mov    -0x10(%ebp),%eax
 924:	8b 00                	mov    (%eax),%eax
 926:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 929:	8b 45 ec             	mov    -0x14(%ebp),%eax
 92c:	8b 40 04             	mov    0x4(%eax),%eax
 92f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 932:	72 4d                	jb     981 <malloc+0xa6>
      if(p->s.size == nunits)
 934:	8b 45 ec             	mov    -0x14(%ebp),%eax
 937:	8b 40 04             	mov    0x4(%eax),%eax
 93a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 93d:	75 0c                	jne    94b <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 93f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 942:	8b 10                	mov    (%eax),%edx
 944:	8b 45 f0             	mov    -0x10(%ebp),%eax
 947:	89 10                	mov    %edx,(%eax)
 949:	eb 26                	jmp    971 <malloc+0x96>
      else {
        p->s.size -= nunits;
 94b:	8b 45 ec             	mov    -0x14(%ebp),%eax
 94e:	8b 40 04             	mov    0x4(%eax),%eax
 951:	89 c2                	mov    %eax,%edx
 953:	2b 55 f4             	sub    -0xc(%ebp),%edx
 956:	8b 45 ec             	mov    -0x14(%ebp),%eax
 959:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 95c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 95f:	8b 40 04             	mov    0x4(%eax),%eax
 962:	c1 e0 03             	shl    $0x3,%eax
 965:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 968:	8b 45 ec             	mov    -0x14(%ebp),%eax
 96b:	8b 55 f4             	mov    -0xc(%ebp),%edx
 96e:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 971:	8b 45 f0             	mov    -0x10(%ebp),%eax
 974:	a3 48 0a 00 00       	mov    %eax,0xa48
      return (void*)(p + 1);
 979:	8b 45 ec             	mov    -0x14(%ebp),%eax
 97c:	83 c0 08             	add    $0x8,%eax
 97f:	eb 38                	jmp    9b9 <malloc+0xde>
    }
    if(p == freep)
 981:	a1 48 0a 00 00       	mov    0xa48,%eax
 986:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 989:	75 1b                	jne    9a6 <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 98b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 98e:	89 04 24             	mov    %eax,(%esp)
 991:	e8 ed fe ff ff       	call   883 <morecore>
 996:	89 45 ec             	mov    %eax,-0x14(%ebp)
 999:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 99d:	75 07                	jne    9a6 <malloc+0xcb>
        return 0;
 99f:	b8 00 00 00 00       	mov    $0x0,%eax
 9a4:	eb 13                	jmp    9b9 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 9a6:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9a9:	89 45 f0             	mov    %eax,-0x10(%ebp)
 9ac:	8b 45 ec             	mov    -0x14(%ebp),%eax
 9af:	8b 00                	mov    (%eax),%eax
 9b1:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 9b4:	e9 70 ff ff ff       	jmp    929 <malloc+0x4e>
}
 9b9:	c9                   	leave  
 9ba:	c3                   	ret    
