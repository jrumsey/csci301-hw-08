
bootblockother.o:     file format elf32-i386


Disassembly of section .text:

00007000 <start>:
#   - it uses the address at start-4, start-8, and start-12

.code16
.globl start
start:
  cli
    7000:	fa                   	cli    

  xorw    %ax,%ax
    7001:	31 c0                	xor    %eax,%eax
  movw    %ax,%ds
    7003:	8e d8                	mov    %eax,%ds
  movw    %ax,%es
    7005:	8e c0                	mov    %eax,%es
  movw    %ax,%ss
    7007:	8e d0                	mov    %eax,%ss

  lgdt    gdtdesc
    7009:	0f 01 16             	lgdtl  (%esi)
    700c:	80 70 0f 20          	xorb   $0x20,0xf(%eax)
  movl    %cr0, %eax
    7010:	c0 66 83 c8          	shlb   $0xc8,-0x7d(%esi)
  orl     $CR0_PE, %eax
    7014:	01 0f                	add    %ecx,(%edi)
  movl    %eax, %cr0
    7016:	22 c0                	and    %al,%al

//PAGEBREAK!
  ljmp    $(SEG_KCODE<<3), $start32
    7018:	ea 1d 70 08 00 66 b8 	ljmp   $0xb866,$0x8701d

0000701d <start32>:

.code32
start32:
  movw    $(SEG_KDATA<<3), %ax
    701d:	66 b8 10 00          	mov    $0x10,%ax
  movw    %ax, %ds
    7021:	8e d8                	mov    %eax,%ds
  movw    %ax, %es
    7023:	8e c0                	mov    %eax,%es
  movw    %ax, %ss
    7025:	8e d0                	mov    %eax,%ss
  movw    $0, %ax
    7027:	66 b8 00 00          	mov    $0x0,%ax
  movw    %ax, %fs
    702b:	8e e0                	mov    %eax,%fs
  movw    %ax, %gs
    702d:	8e e8                	mov    %eax,%gs

  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
    702f:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
    7032:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
    7035:	0f 22 e0             	mov    %eax,%cr4
  # Use enterpgdir as our initial page table
  movl    (start-12), %eax
    7038:	a1 f4 6f 00 00       	mov    0x6ff4,%eax
  movl    %eax, %cr3
    703d:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
    7040:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
    7043:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
    7048:	0f 22 c0             	mov    %eax,%cr0

  # Switch to the stack allocated by startothers()
  movl    (start-4), %esp
    704b:	8b 25 fc 6f 00 00    	mov    0x6ffc,%esp
  # Call mpenter()
  call	  *(start-8)
    7051:	ff 15 f8 6f 00 00    	call   *0x6ff8

  movw    $0x8a00, %ax
    7057:	66 b8 00 8a          	mov    $0x8a00,%ax
  movw    %ax, %dx
    705b:	66 89 c2             	mov    %ax,%dx
  outw    %ax, %dx
    705e:	66 ef                	out    %ax,(%dx)
  movw    $0x8ae0, %ax
    7060:	66 b8 e0 8a          	mov    $0x8ae0,%ax
  outw    %ax, %dx
    7064:	66 ef                	out    %ax,(%dx)

00007066 <spin>:
spin:
  jmp     spin
    7066:	eb fe                	jmp    7066 <spin>

00007068 <gdt>:
	...
    7070:	ff                   	(bad)  
    7071:	ff 00                	incl   (%eax)
    7073:	00 00                	add    %al,(%eax)
    7075:	9a cf 00 ff ff 00 00 	lcall  $0x0,$0xffff00cf
    707c:	00 92 cf 00 17 00    	add    %dl,0x1700cf(%edx)

00007080 <gdtdesc>:
    7080:	17                   	pop    %ss
    7081:	00 68 70             	add    %ch,0x70(%eax)
	...
