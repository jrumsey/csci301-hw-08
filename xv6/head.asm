
_head:     file format elf32-i386


Disassembly of section .text:

00000000 <head>:

char buf[512];

void
head(char* filename)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 38             	sub    $0x38,%esp
  int f, count, readFile;
  char *newLine, *bufCP;
  
  if((f = open(filename, O_RDONLY)) < 0) {
   6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
   d:	00 
   e:	8b 45 08             	mov    0x8(%ebp),%eax
  11:	89 04 24             	mov    %eax,(%esp)
  14:	e8 b7 03 00 00       	call   3d0 <open>
  19:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  1c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  20:	79 20                	jns    42 <head+0x42>
    printf(1, "head: Error, cannot open %s\n", filename);
  22:	8b 45 08             	mov    0x8(%ebp),%eax
  25:	89 44 24 08          	mov    %eax,0x8(%esp)
  29:	c7 44 24 04 d3 08 00 	movl   $0x8d3,0x4(%esp)
  30:	00 
  31:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  38:	e8 cf 04 00 00       	call   50c <printf>
    exit();
  3d:	e8 4e 03 00 00       	call   390 <exit>
  }

  count = 0;
  42:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
  while((readFile = read(f, buf, sizeof(buf))) > 0){
  49:	eb 6a                	jmp    b5 <head+0xb5>
    bufCP = buf;
  4b:	c7 45 f4 40 09 00 00 	movl   $0x940,-0xc(%ebp)
    while((newLine = strchr(bufCP, '\n')) != 0){
  52:	eb 45                	jmp    99 <head+0x99>
      *newLine = '\n';
  54:	8b 45 f0             	mov    -0x10(%ebp),%eax
  57:	c6 00 0a             	movb   $0xa,(%eax)
      write(1, bufCP, newLine-bufCP+1);
  5a:	8b 55 f0             	mov    -0x10(%ebp),%edx
  5d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  60:	89 d1                	mov    %edx,%ecx
  62:	29 c1                	sub    %eax,%ecx
  64:	89 c8                	mov    %ecx,%eax
  66:	83 c0 01             	add    $0x1,%eax
  69:	89 44 24 08          	mov    %eax,0x8(%esp)
  6d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  70:	89 44 24 04          	mov    %eax,0x4(%esp)
  74:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  7b:	e8 30 03 00 00       	call   3b0 <write>
      *newLine = 0;
  80:	8b 45 f0             	mov    -0x10(%ebp),%eax
  83:	c6 00 00             	movb   $0x0,(%eax)
      bufCP = newLine + 1;
  86:	8b 45 f0             	mov    -0x10(%ebp),%eax
  89:	83 c0 01             	add    $0x1,%eax
  8c:	89 45 f4             	mov    %eax,-0xc(%ebp)
      count++;
  8f:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
      if(count > 9)
  93:	83 7d e8 09          	cmpl   $0x9,-0x18(%ebp)
  97:	7f 51                	jg     ea <head+0xea>
  }

  count = 0;
  while((readFile = read(f, buf, sizeof(buf))) > 0){
    bufCP = buf;
    while((newLine = strchr(bufCP, '\n')) != 0){
  99:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
  a0:	00 
  a1:	8b 45 f4             	mov    -0xc(%ebp),%eax
  a4:	89 04 24             	mov    %eax,(%esp)
  a7:	e8 62 01 00 00       	call   20e <strchr>
  ac:	89 45 f0             	mov    %eax,-0x10(%ebp)
  af:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  b3:	75 9f                	jne    54 <head+0x54>
    printf(1, "head: Error, cannot open %s\n", filename);
    exit();
  }

  count = 0;
  while((readFile = read(f, buf, sizeof(buf))) > 0){
  b5:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
  bc:	00 
  bd:	c7 44 24 04 40 09 00 	movl   $0x940,0x4(%esp)
  c4:	00 
  c5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  c8:	89 04 24             	mov    %eax,(%esp)
  cb:	e8 d8 02 00 00       	call   3a8 <read>
  d0:	89 45 ec             	mov    %eax,-0x14(%ebp)
  d3:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
  d7:	0f 8f 6e ff ff ff    	jg     4b <head+0x4b>
      count++;
      if(count > 9)
        return;
    }
  }
  close(f);
  dd:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  e0:	89 04 24             	mov    %eax,(%esp)
  e3:	e8 d0 02 00 00       	call   3b8 <close>
  e8:	eb 01                	jmp    eb <head+0xeb>
      write(1, bufCP, newLine-bufCP+1);
      *newLine = 0;
      bufCP = newLine + 1;
      count++;
      if(count > 9)
        return;
  ea:	90                   	nop
    }
  }
  close(f);
}
  eb:	c9                   	leave  
  ec:	c3                   	ret    

000000ed <main>:

int
main(int argc, char *argv[])
{
  ed:	55                   	push   %ebp
  ee:	89 e5                	mov    %esp,%ebp
  f0:	83 e4 f0             	and    $0xfffffff0,%esp
  f3:	83 ec 10             	sub    $0x10,%esp
    
  if(argc != 2){
  f6:	83 7d 08 02          	cmpl   $0x2,0x8(%ebp)
  fa:	74 19                	je     115 <main+0x28>
    printf(1, "");
  fc:	c7 44 24 04 f0 08 00 	movl   $0x8f0,0x4(%esp)
 103:	00 
 104:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
 10b:	e8 fc 03 00 00       	call   50c <printf>
    exit();
 110:	e8 7b 02 00 00       	call   390 <exit>
  }

  head(argv[1]);
 115:	8b 45 0c             	mov    0xc(%ebp),%eax
 118:	83 c0 04             	add    $0x4,%eax
 11b:	8b 00                	mov    (%eax),%eax
 11d:	89 04 24             	mov    %eax,(%esp)
 120:	e8 db fe ff ff       	call   0 <head>
  exit();
 125:	e8 66 02 00 00       	call   390 <exit>
 12a:	90                   	nop
 12b:	90                   	nop

0000012c <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 12c:	55                   	push   %ebp
 12d:	89 e5                	mov    %esp,%ebp
 12f:	57                   	push   %edi
 130:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 131:	8b 4d 08             	mov    0x8(%ebp),%ecx
 134:	8b 55 10             	mov    0x10(%ebp),%edx
 137:	8b 45 0c             	mov    0xc(%ebp),%eax
 13a:	89 cb                	mov    %ecx,%ebx
 13c:	89 df                	mov    %ebx,%edi
 13e:	89 d1                	mov    %edx,%ecx
 140:	fc                   	cld    
 141:	f3 aa                	rep stos %al,%es:(%edi)
 143:	89 ca                	mov    %ecx,%edx
 145:	89 fb                	mov    %edi,%ebx
 147:	89 5d 08             	mov    %ebx,0x8(%ebp)
 14a:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 14d:	5b                   	pop    %ebx
 14e:	5f                   	pop    %edi
 14f:	5d                   	pop    %ebp
 150:	c3                   	ret    

00000151 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 151:	55                   	push   %ebp
 152:	89 e5                	mov    %esp,%ebp
 154:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 157:	8b 45 08             	mov    0x8(%ebp),%eax
 15a:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 15d:	8b 45 0c             	mov    0xc(%ebp),%eax
 160:	0f b6 10             	movzbl (%eax),%edx
 163:	8b 45 08             	mov    0x8(%ebp),%eax
 166:	88 10                	mov    %dl,(%eax)
 168:	8b 45 08             	mov    0x8(%ebp),%eax
 16b:	0f b6 00             	movzbl (%eax),%eax
 16e:	84 c0                	test   %al,%al
 170:	0f 95 c0             	setne  %al
 173:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 177:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 17b:	84 c0                	test   %al,%al
 17d:	75 de                	jne    15d <strcpy+0xc>
    ;
  return os;
 17f:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 182:	c9                   	leave  
 183:	c3                   	ret    

00000184 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 184:	55                   	push   %ebp
 185:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 187:	eb 08                	jmp    191 <strcmp+0xd>
    p++, q++;
 189:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 18d:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 191:	8b 45 08             	mov    0x8(%ebp),%eax
 194:	0f b6 00             	movzbl (%eax),%eax
 197:	84 c0                	test   %al,%al
 199:	74 10                	je     1ab <strcmp+0x27>
 19b:	8b 45 08             	mov    0x8(%ebp),%eax
 19e:	0f b6 10             	movzbl (%eax),%edx
 1a1:	8b 45 0c             	mov    0xc(%ebp),%eax
 1a4:	0f b6 00             	movzbl (%eax),%eax
 1a7:	38 c2                	cmp    %al,%dl
 1a9:	74 de                	je     189 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 1ab:	8b 45 08             	mov    0x8(%ebp),%eax
 1ae:	0f b6 00             	movzbl (%eax),%eax
 1b1:	0f b6 d0             	movzbl %al,%edx
 1b4:	8b 45 0c             	mov    0xc(%ebp),%eax
 1b7:	0f b6 00             	movzbl (%eax),%eax
 1ba:	0f b6 c0             	movzbl %al,%eax
 1bd:	89 d1                	mov    %edx,%ecx
 1bf:	29 c1                	sub    %eax,%ecx
 1c1:	89 c8                	mov    %ecx,%eax
}
 1c3:	5d                   	pop    %ebp
 1c4:	c3                   	ret    

000001c5 <strlen>:

uint
strlen(char *s)
{
 1c5:	55                   	push   %ebp
 1c6:	89 e5                	mov    %esp,%ebp
 1c8:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1cb:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1d2:	eb 04                	jmp    1d8 <strlen+0x13>
 1d4:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1d8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 1db:	03 45 08             	add    0x8(%ebp),%eax
 1de:	0f b6 00             	movzbl (%eax),%eax
 1e1:	84 c0                	test   %al,%al
 1e3:	75 ef                	jne    1d4 <strlen+0xf>
    ;
  return n;
 1e5:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1e8:	c9                   	leave  
 1e9:	c3                   	ret    

000001ea <memset>:

void*
memset(void *dst, int c, uint n)
{
 1ea:	55                   	push   %ebp
 1eb:	89 e5                	mov    %esp,%ebp
 1ed:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 1f0:	8b 45 10             	mov    0x10(%ebp),%eax
 1f3:	89 44 24 08          	mov    %eax,0x8(%esp)
 1f7:	8b 45 0c             	mov    0xc(%ebp),%eax
 1fa:	89 44 24 04          	mov    %eax,0x4(%esp)
 1fe:	8b 45 08             	mov    0x8(%ebp),%eax
 201:	89 04 24             	mov    %eax,(%esp)
 204:	e8 23 ff ff ff       	call   12c <stosb>
  return dst;
 209:	8b 45 08             	mov    0x8(%ebp),%eax
}
 20c:	c9                   	leave  
 20d:	c3                   	ret    

0000020e <strchr>:

char*
strchr(const char *s, char c)
{
 20e:	55                   	push   %ebp
 20f:	89 e5                	mov    %esp,%ebp
 211:	83 ec 04             	sub    $0x4,%esp
 214:	8b 45 0c             	mov    0xc(%ebp),%eax
 217:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 21a:	eb 14                	jmp    230 <strchr+0x22>
    if(*s == c)
 21c:	8b 45 08             	mov    0x8(%ebp),%eax
 21f:	0f b6 00             	movzbl (%eax),%eax
 222:	3a 45 fc             	cmp    -0x4(%ebp),%al
 225:	75 05                	jne    22c <strchr+0x1e>
      return (char*)s;
 227:	8b 45 08             	mov    0x8(%ebp),%eax
 22a:	eb 13                	jmp    23f <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 22c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 230:	8b 45 08             	mov    0x8(%ebp),%eax
 233:	0f b6 00             	movzbl (%eax),%eax
 236:	84 c0                	test   %al,%al
 238:	75 e2                	jne    21c <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 23a:	b8 00 00 00 00       	mov    $0x0,%eax
}
 23f:	c9                   	leave  
 240:	c3                   	ret    

00000241 <gets>:

char*
gets(char *buf, int max)
{
 241:	55                   	push   %ebp
 242:	89 e5                	mov    %esp,%ebp
 244:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 247:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 24e:	eb 44                	jmp    294 <gets+0x53>
    cc = read(0, &c, 1);
 250:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 257:	00 
 258:	8d 45 ef             	lea    -0x11(%ebp),%eax
 25b:	89 44 24 04          	mov    %eax,0x4(%esp)
 25f:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 266:	e8 3d 01 00 00       	call   3a8 <read>
 26b:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 26e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 272:	7e 2d                	jle    2a1 <gets+0x60>
      break;
    buf[i++] = c;
 274:	8b 45 f0             	mov    -0x10(%ebp),%eax
 277:	03 45 08             	add    0x8(%ebp),%eax
 27a:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 27e:	88 10                	mov    %dl,(%eax)
 280:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 284:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 288:	3c 0a                	cmp    $0xa,%al
 28a:	74 16                	je     2a2 <gets+0x61>
 28c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 290:	3c 0d                	cmp    $0xd,%al
 292:	74 0e                	je     2a2 <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 294:	8b 45 f0             	mov    -0x10(%ebp),%eax
 297:	83 c0 01             	add    $0x1,%eax
 29a:	3b 45 0c             	cmp    0xc(%ebp),%eax
 29d:	7c b1                	jl     250 <gets+0xf>
 29f:	eb 01                	jmp    2a2 <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 2a1:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 2a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2a5:	03 45 08             	add    0x8(%ebp),%eax
 2a8:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 2ab:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2ae:	c9                   	leave  
 2af:	c3                   	ret    

000002b0 <stat>:

int
stat(char *n, struct stat *st)
{
 2b0:	55                   	push   %ebp
 2b1:	89 e5                	mov    %esp,%ebp
 2b3:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 2b6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 2bd:	00 
 2be:	8b 45 08             	mov    0x8(%ebp),%eax
 2c1:	89 04 24             	mov    %eax,(%esp)
 2c4:	e8 07 01 00 00       	call   3d0 <open>
 2c9:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 2cc:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 2d0:	79 07                	jns    2d9 <stat+0x29>
    return -1;
 2d2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2d7:	eb 23                	jmp    2fc <stat+0x4c>
  r = fstat(fd, st);
 2d9:	8b 45 0c             	mov    0xc(%ebp),%eax
 2dc:	89 44 24 04          	mov    %eax,0x4(%esp)
 2e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2e3:	89 04 24             	mov    %eax,(%esp)
 2e6:	e8 fd 00 00 00       	call   3e8 <fstat>
 2eb:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 2ee:	8b 45 f0             	mov    -0x10(%ebp),%eax
 2f1:	89 04 24             	mov    %eax,(%esp)
 2f4:	e8 bf 00 00 00       	call   3b8 <close>
  return r;
 2f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 2fc:	c9                   	leave  
 2fd:	c3                   	ret    

000002fe <atoi>:

int
atoi(const char *s)
{
 2fe:	55                   	push   %ebp
 2ff:	89 e5                	mov    %esp,%ebp
 301:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 304:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 30b:	eb 24                	jmp    331 <atoi+0x33>
    n = n*10 + *s++ - '0';
 30d:	8b 55 fc             	mov    -0x4(%ebp),%edx
 310:	89 d0                	mov    %edx,%eax
 312:	c1 e0 02             	shl    $0x2,%eax
 315:	01 d0                	add    %edx,%eax
 317:	01 c0                	add    %eax,%eax
 319:	89 c2                	mov    %eax,%edx
 31b:	8b 45 08             	mov    0x8(%ebp),%eax
 31e:	0f b6 00             	movzbl (%eax),%eax
 321:	0f be c0             	movsbl %al,%eax
 324:	8d 04 02             	lea    (%edx,%eax,1),%eax
 327:	83 e8 30             	sub    $0x30,%eax
 32a:	89 45 fc             	mov    %eax,-0x4(%ebp)
 32d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 331:	8b 45 08             	mov    0x8(%ebp),%eax
 334:	0f b6 00             	movzbl (%eax),%eax
 337:	3c 2f                	cmp    $0x2f,%al
 339:	7e 0a                	jle    345 <atoi+0x47>
 33b:	8b 45 08             	mov    0x8(%ebp),%eax
 33e:	0f b6 00             	movzbl (%eax),%eax
 341:	3c 39                	cmp    $0x39,%al
 343:	7e c8                	jle    30d <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 345:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 348:	c9                   	leave  
 349:	c3                   	ret    

0000034a <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 34a:	55                   	push   %ebp
 34b:	89 e5                	mov    %esp,%ebp
 34d:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 350:	8b 45 08             	mov    0x8(%ebp),%eax
 353:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 356:	8b 45 0c             	mov    0xc(%ebp),%eax
 359:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 35c:	eb 13                	jmp    371 <memmove+0x27>
    *dst++ = *src++;
 35e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 361:	0f b6 10             	movzbl (%eax),%edx
 364:	8b 45 f8             	mov    -0x8(%ebp),%eax
 367:	88 10                	mov    %dl,(%eax)
 369:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 36d:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 371:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 375:	0f 9f c0             	setg   %al
 378:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 37c:	84 c0                	test   %al,%al
 37e:	75 de                	jne    35e <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 380:	8b 45 08             	mov    0x8(%ebp),%eax
}
 383:	c9                   	leave  
 384:	c3                   	ret    
 385:	90                   	nop
 386:	90                   	nop
 387:	90                   	nop

00000388 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 388:	b8 01 00 00 00       	mov    $0x1,%eax
 38d:	cd 40                	int    $0x40
 38f:	c3                   	ret    

00000390 <exit>:
SYSCALL(exit)
 390:	b8 02 00 00 00       	mov    $0x2,%eax
 395:	cd 40                	int    $0x40
 397:	c3                   	ret    

00000398 <wait>:
SYSCALL(wait)
 398:	b8 03 00 00 00       	mov    $0x3,%eax
 39d:	cd 40                	int    $0x40
 39f:	c3                   	ret    

000003a0 <pipe>:
SYSCALL(pipe)
 3a0:	b8 04 00 00 00       	mov    $0x4,%eax
 3a5:	cd 40                	int    $0x40
 3a7:	c3                   	ret    

000003a8 <read>:
SYSCALL(read)
 3a8:	b8 05 00 00 00       	mov    $0x5,%eax
 3ad:	cd 40                	int    $0x40
 3af:	c3                   	ret    

000003b0 <write>:
SYSCALL(write)
 3b0:	b8 10 00 00 00       	mov    $0x10,%eax
 3b5:	cd 40                	int    $0x40
 3b7:	c3                   	ret    

000003b8 <close>:
SYSCALL(close)
 3b8:	b8 15 00 00 00       	mov    $0x15,%eax
 3bd:	cd 40                	int    $0x40
 3bf:	c3                   	ret    

000003c0 <kill>:
SYSCALL(kill)
 3c0:	b8 06 00 00 00       	mov    $0x6,%eax
 3c5:	cd 40                	int    $0x40
 3c7:	c3                   	ret    

000003c8 <exec>:
SYSCALL(exec)
 3c8:	b8 07 00 00 00       	mov    $0x7,%eax
 3cd:	cd 40                	int    $0x40
 3cf:	c3                   	ret    

000003d0 <open>:
SYSCALL(open)
 3d0:	b8 0f 00 00 00       	mov    $0xf,%eax
 3d5:	cd 40                	int    $0x40
 3d7:	c3                   	ret    

000003d8 <mknod>:
SYSCALL(mknod)
 3d8:	b8 11 00 00 00       	mov    $0x11,%eax
 3dd:	cd 40                	int    $0x40
 3df:	c3                   	ret    

000003e0 <unlink>:
SYSCALL(unlink)
 3e0:	b8 12 00 00 00       	mov    $0x12,%eax
 3e5:	cd 40                	int    $0x40
 3e7:	c3                   	ret    

000003e8 <fstat>:
SYSCALL(fstat)
 3e8:	b8 08 00 00 00       	mov    $0x8,%eax
 3ed:	cd 40                	int    $0x40
 3ef:	c3                   	ret    

000003f0 <link>:
SYSCALL(link)
 3f0:	b8 13 00 00 00       	mov    $0x13,%eax
 3f5:	cd 40                	int    $0x40
 3f7:	c3                   	ret    

000003f8 <mkdir>:
SYSCALL(mkdir)
 3f8:	b8 14 00 00 00       	mov    $0x14,%eax
 3fd:	cd 40                	int    $0x40
 3ff:	c3                   	ret    

00000400 <chdir>:
SYSCALL(chdir)
 400:	b8 09 00 00 00       	mov    $0x9,%eax
 405:	cd 40                	int    $0x40
 407:	c3                   	ret    

00000408 <dup>:
SYSCALL(dup)
 408:	b8 0a 00 00 00       	mov    $0xa,%eax
 40d:	cd 40                	int    $0x40
 40f:	c3                   	ret    

00000410 <getpid>:
SYSCALL(getpid)
 410:	b8 0b 00 00 00       	mov    $0xb,%eax
 415:	cd 40                	int    $0x40
 417:	c3                   	ret    

00000418 <sbrk>:
SYSCALL(sbrk)
 418:	b8 0c 00 00 00       	mov    $0xc,%eax
 41d:	cd 40                	int    $0x40
 41f:	c3                   	ret    

00000420 <sleep>:
SYSCALL(sleep)
 420:	b8 0d 00 00 00       	mov    $0xd,%eax
 425:	cd 40                	int    $0x40
 427:	c3                   	ret    

00000428 <uptime>:
SYSCALL(uptime)
 428:	b8 0e 00 00 00       	mov    $0xe,%eax
 42d:	cd 40                	int    $0x40
 42f:	c3                   	ret    

00000430 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 430:	55                   	push   %ebp
 431:	89 e5                	mov    %esp,%ebp
 433:	83 ec 28             	sub    $0x28,%esp
 436:	8b 45 0c             	mov    0xc(%ebp),%eax
 439:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 43c:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 443:	00 
 444:	8d 45 f4             	lea    -0xc(%ebp),%eax
 447:	89 44 24 04          	mov    %eax,0x4(%esp)
 44b:	8b 45 08             	mov    0x8(%ebp),%eax
 44e:	89 04 24             	mov    %eax,(%esp)
 451:	e8 5a ff ff ff       	call   3b0 <write>
}
 456:	c9                   	leave  
 457:	c3                   	ret    

00000458 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 458:	55                   	push   %ebp
 459:	89 e5                	mov    %esp,%ebp
 45b:	53                   	push   %ebx
 45c:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 45f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 466:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 46a:	74 17                	je     483 <printint+0x2b>
 46c:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 470:	79 11                	jns    483 <printint+0x2b>
    neg = 1;
 472:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 479:	8b 45 0c             	mov    0xc(%ebp),%eax
 47c:	f7 d8                	neg    %eax
 47e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 481:	eb 06                	jmp    489 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 483:	8b 45 0c             	mov    0xc(%ebp),%eax
 486:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 489:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 490:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 493:	8b 5d 10             	mov    0x10(%ebp),%ebx
 496:	8b 45 f4             	mov    -0xc(%ebp),%eax
 499:	ba 00 00 00 00       	mov    $0x0,%edx
 49e:	f7 f3                	div    %ebx
 4a0:	89 d0                	mov    %edx,%eax
 4a2:	0f b6 80 f8 08 00 00 	movzbl 0x8f8(%eax),%eax
 4a9:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 4ad:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 4b1:	8b 45 10             	mov    0x10(%ebp),%eax
 4b4:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 4b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4ba:	ba 00 00 00 00       	mov    $0x0,%edx
 4bf:	f7 75 d4             	divl   -0x2c(%ebp)
 4c2:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4c5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 4c9:	75 c5                	jne    490 <printint+0x38>
  if(neg)
 4cb:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4cf:	74 2a                	je     4fb <printint+0xa3>
    buf[i++] = '-';
 4d1:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4d4:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 4d9:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 4dd:	eb 1d                	jmp    4fc <printint+0xa4>
    putc(fd, buf[i]);
 4df:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4e2:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 4e7:	0f be c0             	movsbl %al,%eax
 4ea:	89 44 24 04          	mov    %eax,0x4(%esp)
 4ee:	8b 45 08             	mov    0x8(%ebp),%eax
 4f1:	89 04 24             	mov    %eax,(%esp)
 4f4:	e8 37 ff ff ff       	call   430 <putc>
 4f9:	eb 01                	jmp    4fc <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 4fb:	90                   	nop
 4fc:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 500:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 504:	79 d9                	jns    4df <printint+0x87>
    putc(fd, buf[i]);
}
 506:	83 c4 44             	add    $0x44,%esp
 509:	5b                   	pop    %ebx
 50a:	5d                   	pop    %ebp
 50b:	c3                   	ret    

0000050c <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 50c:	55                   	push   %ebp
 50d:	89 e5                	mov    %esp,%ebp
 50f:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 512:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 519:	8d 45 0c             	lea    0xc(%ebp),%eax
 51c:	83 c0 04             	add    $0x4,%eax
 51f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 522:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 529:	e9 7e 01 00 00       	jmp    6ac <printf+0x1a0>
    c = fmt[i] & 0xff;
 52e:	8b 55 0c             	mov    0xc(%ebp),%edx
 531:	8b 45 ec             	mov    -0x14(%ebp),%eax
 534:	8d 04 02             	lea    (%edx,%eax,1),%eax
 537:	0f b6 00             	movzbl (%eax),%eax
 53a:	0f be c0             	movsbl %al,%eax
 53d:	25 ff 00 00 00       	and    $0xff,%eax
 542:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 545:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 549:	75 2c                	jne    577 <printf+0x6b>
      if(c == '%'){
 54b:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 54f:	75 0c                	jne    55d <printf+0x51>
        state = '%';
 551:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 558:	e9 4b 01 00 00       	jmp    6a8 <printf+0x19c>
      } else {
        putc(fd, c);
 55d:	8b 45 e8             	mov    -0x18(%ebp),%eax
 560:	0f be c0             	movsbl %al,%eax
 563:	89 44 24 04          	mov    %eax,0x4(%esp)
 567:	8b 45 08             	mov    0x8(%ebp),%eax
 56a:	89 04 24             	mov    %eax,(%esp)
 56d:	e8 be fe ff ff       	call   430 <putc>
 572:	e9 31 01 00 00       	jmp    6a8 <printf+0x19c>
      }
    } else if(state == '%'){
 577:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 57b:	0f 85 27 01 00 00    	jne    6a8 <printf+0x19c>
      if(c == 'd'){
 581:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 585:	75 2d                	jne    5b4 <printf+0xa8>
        printint(fd, *ap, 10, 1);
 587:	8b 45 f4             	mov    -0xc(%ebp),%eax
 58a:	8b 00                	mov    (%eax),%eax
 58c:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 593:	00 
 594:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 59b:	00 
 59c:	89 44 24 04          	mov    %eax,0x4(%esp)
 5a0:	8b 45 08             	mov    0x8(%ebp),%eax
 5a3:	89 04 24             	mov    %eax,(%esp)
 5a6:	e8 ad fe ff ff       	call   458 <printint>
        ap++;
 5ab:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 5af:	e9 ed 00 00 00       	jmp    6a1 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 5b4:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 5b8:	74 06                	je     5c0 <printf+0xb4>
 5ba:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 5be:	75 2d                	jne    5ed <printf+0xe1>
        printint(fd, *ap, 16, 0);
 5c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5c3:	8b 00                	mov    (%eax),%eax
 5c5:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 5cc:	00 
 5cd:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 5d4:	00 
 5d5:	89 44 24 04          	mov    %eax,0x4(%esp)
 5d9:	8b 45 08             	mov    0x8(%ebp),%eax
 5dc:	89 04 24             	mov    %eax,(%esp)
 5df:	e8 74 fe ff ff       	call   458 <printint>
        ap++;
 5e4:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 5e8:	e9 b4 00 00 00       	jmp    6a1 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 5ed:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 5f1:	75 46                	jne    639 <printf+0x12d>
        s = (char*)*ap;
 5f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5f6:	8b 00                	mov    (%eax),%eax
 5f8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 5fb:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 5ff:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 603:	75 27                	jne    62c <printf+0x120>
          s = "(null)";
 605:	c7 45 e4 f1 08 00 00 	movl   $0x8f1,-0x1c(%ebp)
        while(*s != 0){
 60c:	eb 1f                	jmp    62d <printf+0x121>
          putc(fd, *s);
 60e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 611:	0f b6 00             	movzbl (%eax),%eax
 614:	0f be c0             	movsbl %al,%eax
 617:	89 44 24 04          	mov    %eax,0x4(%esp)
 61b:	8b 45 08             	mov    0x8(%ebp),%eax
 61e:	89 04 24             	mov    %eax,(%esp)
 621:	e8 0a fe ff ff       	call   430 <putc>
          s++;
 626:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 62a:	eb 01                	jmp    62d <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 62c:	90                   	nop
 62d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 630:	0f b6 00             	movzbl (%eax),%eax
 633:	84 c0                	test   %al,%al
 635:	75 d7                	jne    60e <printf+0x102>
 637:	eb 68                	jmp    6a1 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 639:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 63d:	75 1d                	jne    65c <printf+0x150>
        putc(fd, *ap);
 63f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 642:	8b 00                	mov    (%eax),%eax
 644:	0f be c0             	movsbl %al,%eax
 647:	89 44 24 04          	mov    %eax,0x4(%esp)
 64b:	8b 45 08             	mov    0x8(%ebp),%eax
 64e:	89 04 24             	mov    %eax,(%esp)
 651:	e8 da fd ff ff       	call   430 <putc>
        ap++;
 656:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 65a:	eb 45                	jmp    6a1 <printf+0x195>
      } else if(c == '%'){
 65c:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 660:	75 17                	jne    679 <printf+0x16d>
        putc(fd, c);
 662:	8b 45 e8             	mov    -0x18(%ebp),%eax
 665:	0f be c0             	movsbl %al,%eax
 668:	89 44 24 04          	mov    %eax,0x4(%esp)
 66c:	8b 45 08             	mov    0x8(%ebp),%eax
 66f:	89 04 24             	mov    %eax,(%esp)
 672:	e8 b9 fd ff ff       	call   430 <putc>
 677:	eb 28                	jmp    6a1 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 679:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 680:	00 
 681:	8b 45 08             	mov    0x8(%ebp),%eax
 684:	89 04 24             	mov    %eax,(%esp)
 687:	e8 a4 fd ff ff       	call   430 <putc>
        putc(fd, c);
 68c:	8b 45 e8             	mov    -0x18(%ebp),%eax
 68f:	0f be c0             	movsbl %al,%eax
 692:	89 44 24 04          	mov    %eax,0x4(%esp)
 696:	8b 45 08             	mov    0x8(%ebp),%eax
 699:	89 04 24             	mov    %eax,(%esp)
 69c:	e8 8f fd ff ff       	call   430 <putc>
      }
      state = 0;
 6a1:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 6a8:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 6ac:	8b 55 0c             	mov    0xc(%ebp),%edx
 6af:	8b 45 ec             	mov    -0x14(%ebp),%eax
 6b2:	8d 04 02             	lea    (%edx,%eax,1),%eax
 6b5:	0f b6 00             	movzbl (%eax),%eax
 6b8:	84 c0                	test   %al,%al
 6ba:	0f 85 6e fe ff ff    	jne    52e <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6c0:	c9                   	leave  
 6c1:	c3                   	ret    
 6c2:	90                   	nop
 6c3:	90                   	nop

000006c4 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6c4:	55                   	push   %ebp
 6c5:	89 e5                	mov    %esp,%ebp
 6c7:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6ca:	8b 45 08             	mov    0x8(%ebp),%eax
 6cd:	83 e8 08             	sub    $0x8,%eax
 6d0:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6d3:	a1 28 09 00 00       	mov    0x928,%eax
 6d8:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6db:	eb 24                	jmp    701 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 6dd:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e0:	8b 00                	mov    (%eax),%eax
 6e2:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6e5:	77 12                	ja     6f9 <free+0x35>
 6e7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6ea:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6ed:	77 24                	ja     713 <free+0x4f>
 6ef:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6f2:	8b 00                	mov    (%eax),%eax
 6f4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6f7:	77 1a                	ja     713 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6f9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6fc:	8b 00                	mov    (%eax),%eax
 6fe:	89 45 fc             	mov    %eax,-0x4(%ebp)
 701:	8b 45 f8             	mov    -0x8(%ebp),%eax
 704:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 707:	76 d4                	jbe    6dd <free+0x19>
 709:	8b 45 fc             	mov    -0x4(%ebp),%eax
 70c:	8b 00                	mov    (%eax),%eax
 70e:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 711:	76 ca                	jbe    6dd <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 713:	8b 45 f8             	mov    -0x8(%ebp),%eax
 716:	8b 40 04             	mov    0x4(%eax),%eax
 719:	c1 e0 03             	shl    $0x3,%eax
 71c:	89 c2                	mov    %eax,%edx
 71e:	03 55 f8             	add    -0x8(%ebp),%edx
 721:	8b 45 fc             	mov    -0x4(%ebp),%eax
 724:	8b 00                	mov    (%eax),%eax
 726:	39 c2                	cmp    %eax,%edx
 728:	75 24                	jne    74e <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 72a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 72d:	8b 50 04             	mov    0x4(%eax),%edx
 730:	8b 45 fc             	mov    -0x4(%ebp),%eax
 733:	8b 00                	mov    (%eax),%eax
 735:	8b 40 04             	mov    0x4(%eax),%eax
 738:	01 c2                	add    %eax,%edx
 73a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 73d:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 740:	8b 45 fc             	mov    -0x4(%ebp),%eax
 743:	8b 00                	mov    (%eax),%eax
 745:	8b 10                	mov    (%eax),%edx
 747:	8b 45 f8             	mov    -0x8(%ebp),%eax
 74a:	89 10                	mov    %edx,(%eax)
 74c:	eb 0a                	jmp    758 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 74e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 751:	8b 10                	mov    (%eax),%edx
 753:	8b 45 f8             	mov    -0x8(%ebp),%eax
 756:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 758:	8b 45 fc             	mov    -0x4(%ebp),%eax
 75b:	8b 40 04             	mov    0x4(%eax),%eax
 75e:	c1 e0 03             	shl    $0x3,%eax
 761:	03 45 fc             	add    -0x4(%ebp),%eax
 764:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 767:	75 20                	jne    789 <free+0xc5>
    p->s.size += bp->s.size;
 769:	8b 45 fc             	mov    -0x4(%ebp),%eax
 76c:	8b 50 04             	mov    0x4(%eax),%edx
 76f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 772:	8b 40 04             	mov    0x4(%eax),%eax
 775:	01 c2                	add    %eax,%edx
 777:	8b 45 fc             	mov    -0x4(%ebp),%eax
 77a:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 77d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 780:	8b 10                	mov    (%eax),%edx
 782:	8b 45 fc             	mov    -0x4(%ebp),%eax
 785:	89 10                	mov    %edx,(%eax)
 787:	eb 08                	jmp    791 <free+0xcd>
  } else
    p->s.ptr = bp;
 789:	8b 45 fc             	mov    -0x4(%ebp),%eax
 78c:	8b 55 f8             	mov    -0x8(%ebp),%edx
 78f:	89 10                	mov    %edx,(%eax)
  freep = p;
 791:	8b 45 fc             	mov    -0x4(%ebp),%eax
 794:	a3 28 09 00 00       	mov    %eax,0x928
}
 799:	c9                   	leave  
 79a:	c3                   	ret    

0000079b <morecore>:

static Header*
morecore(uint nu)
{
 79b:	55                   	push   %ebp
 79c:	89 e5                	mov    %esp,%ebp
 79e:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 7a1:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 7a8:	77 07                	ja     7b1 <morecore+0x16>
    nu = 4096;
 7aa:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7b1:	8b 45 08             	mov    0x8(%ebp),%eax
 7b4:	c1 e0 03             	shl    $0x3,%eax
 7b7:	89 04 24             	mov    %eax,(%esp)
 7ba:	e8 59 fc ff ff       	call   418 <sbrk>
 7bf:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 7c2:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 7c6:	75 07                	jne    7cf <morecore+0x34>
    return 0;
 7c8:	b8 00 00 00 00       	mov    $0x0,%eax
 7cd:	eb 22                	jmp    7f1 <morecore+0x56>
  hp = (Header*)p;
 7cf:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7d2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 7d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7d8:	8b 55 08             	mov    0x8(%ebp),%edx
 7db:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 7de:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7e1:	83 c0 08             	add    $0x8,%eax
 7e4:	89 04 24             	mov    %eax,(%esp)
 7e7:	e8 d8 fe ff ff       	call   6c4 <free>
  return freep;
 7ec:	a1 28 09 00 00       	mov    0x928,%eax
}
 7f1:	c9                   	leave  
 7f2:	c3                   	ret    

000007f3 <malloc>:

void*
malloc(uint nbytes)
{
 7f3:	55                   	push   %ebp
 7f4:	89 e5                	mov    %esp,%ebp
 7f6:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7f9:	8b 45 08             	mov    0x8(%ebp),%eax
 7fc:	83 c0 07             	add    $0x7,%eax
 7ff:	c1 e8 03             	shr    $0x3,%eax
 802:	83 c0 01             	add    $0x1,%eax
 805:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 808:	a1 28 09 00 00       	mov    0x928,%eax
 80d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 810:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 814:	75 23                	jne    839 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 816:	c7 45 f0 20 09 00 00 	movl   $0x920,-0x10(%ebp)
 81d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 820:	a3 28 09 00 00       	mov    %eax,0x928
 825:	a1 28 09 00 00       	mov    0x928,%eax
 82a:	a3 20 09 00 00       	mov    %eax,0x920
    base.s.size = 0;
 82f:	c7 05 24 09 00 00 00 	movl   $0x0,0x924
 836:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 839:	8b 45 f0             	mov    -0x10(%ebp),%eax
 83c:	8b 00                	mov    (%eax),%eax
 83e:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 841:	8b 45 ec             	mov    -0x14(%ebp),%eax
 844:	8b 40 04             	mov    0x4(%eax),%eax
 847:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 84a:	72 4d                	jb     899 <malloc+0xa6>
      if(p->s.size == nunits)
 84c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 84f:	8b 40 04             	mov    0x4(%eax),%eax
 852:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 855:	75 0c                	jne    863 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 857:	8b 45 ec             	mov    -0x14(%ebp),%eax
 85a:	8b 10                	mov    (%eax),%edx
 85c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 85f:	89 10                	mov    %edx,(%eax)
 861:	eb 26                	jmp    889 <malloc+0x96>
      else {
        p->s.size -= nunits;
 863:	8b 45 ec             	mov    -0x14(%ebp),%eax
 866:	8b 40 04             	mov    0x4(%eax),%eax
 869:	89 c2                	mov    %eax,%edx
 86b:	2b 55 f4             	sub    -0xc(%ebp),%edx
 86e:	8b 45 ec             	mov    -0x14(%ebp),%eax
 871:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 874:	8b 45 ec             	mov    -0x14(%ebp),%eax
 877:	8b 40 04             	mov    0x4(%eax),%eax
 87a:	c1 e0 03             	shl    $0x3,%eax
 87d:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 880:	8b 45 ec             	mov    -0x14(%ebp),%eax
 883:	8b 55 f4             	mov    -0xc(%ebp),%edx
 886:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 889:	8b 45 f0             	mov    -0x10(%ebp),%eax
 88c:	a3 28 09 00 00       	mov    %eax,0x928
      return (void*)(p + 1);
 891:	8b 45 ec             	mov    -0x14(%ebp),%eax
 894:	83 c0 08             	add    $0x8,%eax
 897:	eb 38                	jmp    8d1 <malloc+0xde>
    }
    if(p == freep)
 899:	a1 28 09 00 00       	mov    0x928,%eax
 89e:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 8a1:	75 1b                	jne    8be <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 8a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8a6:	89 04 24             	mov    %eax,(%esp)
 8a9:	e8 ed fe ff ff       	call   79b <morecore>
 8ae:	89 45 ec             	mov    %eax,-0x14(%ebp)
 8b1:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 8b5:	75 07                	jne    8be <malloc+0xcb>
        return 0;
 8b7:	b8 00 00 00 00       	mov    $0x0,%eax
 8bc:	eb 13                	jmp    8d1 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8be:	8b 45 ec             	mov    -0x14(%ebp),%eax
 8c1:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8c4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 8c7:	8b 00                	mov    (%eax),%eax
 8c9:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 8cc:	e9 70 ff ff ff       	jmp    841 <malloc+0x4e>
}
 8d1:	c9                   	leave  
 8d2:	c3                   	ret    
