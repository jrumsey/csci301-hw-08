
_mv:     file format elf32-i386


Disassembly of section .text:

00000000 <mv>:

char buf[512];

void
mv(char* filename, char* filenamenew)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 28             	sub    $0x28,%esp
  int f, fn;
  if((f = link(filename, filenamenew) < 0) || (fn = unlink(filename) < 0)) {
   6:	8b 45 0c             	mov    0xc(%ebp),%eax
   9:	89 44 24 04          	mov    %eax,0x4(%esp)
   d:	8b 45 08             	mov    0x8(%ebp),%eax
  10:	89 04 24             	mov    %eax,(%esp)
  13:	e8 40 03 00 00       	call   358 <link>
  18:	c1 e8 1f             	shr    $0x1f,%eax
  1b:	89 45 f0             	mov    %eax,-0x10(%ebp)
  1e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  22:	75 17                	jne    3b <mv+0x3b>
  24:	8b 45 08             	mov    0x8(%ebp),%eax
  27:	89 04 24             	mov    %eax,(%esp)
  2a:	e8 19 03 00 00       	call   348 <unlink>
  2f:	c1 e8 1f             	shr    $0x1f,%eax
  32:	89 45 f4             	mov    %eax,-0xc(%ebp)
  35:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  39:	74 22                	je     5d <mv+0x5d>
    printf(1, "mv: Error, cannot move %s to %s\n", filename, filenamenew);
  3b:	8b 45 0c             	mov    0xc(%ebp),%eax
  3e:	89 44 24 0c          	mov    %eax,0xc(%esp)
  42:	8b 45 08             	mov    0x8(%ebp),%eax
  45:	89 44 24 08          	mov    %eax,0x8(%esp)
  49:	c7 44 24 04 3c 08 00 	movl   $0x83c,0x4(%esp)
  50:	00 
  51:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  58:	e8 17 04 00 00       	call   474 <printf>
  }
//  close(f);
//  close(fn);
}
  5d:	c9                   	leave  
  5e:	c3                   	ret    

0000005f <main>:

int
main(int argc, char *argv[])
{
  5f:	55                   	push   %ebp
  60:	89 e5                	mov    %esp,%ebp
  62:	83 e4 f0             	and    $0xfffffff0,%esp
  65:	83 ec 10             	sub    $0x10,%esp
  //int fd, i;

  if(argc <= 1){ 
  68:	83 7d 08 01          	cmpl   $0x1,0x8(%ebp)
  6c:	7f 05                	jg     73 <main+0x14>
    exit();
  6e:	e8 85 02 00 00       	call   2f8 <exit>
  }

    mv(argv[1], argv[2]);
  73:	8b 45 0c             	mov    0xc(%ebp),%eax
  76:	83 c0 08             	add    $0x8,%eax
  79:	8b 10                	mov    (%eax),%edx
  7b:	8b 45 0c             	mov    0xc(%ebp),%eax
  7e:	83 c0 04             	add    $0x4,%eax
  81:	8b 00                	mov    (%eax),%eax
  83:	89 54 24 04          	mov    %edx,0x4(%esp)
  87:	89 04 24             	mov    %eax,(%esp)
  8a:	e8 71 ff ff ff       	call   0 <mv>
  
  exit();
  8f:	e8 64 02 00 00       	call   2f8 <exit>

00000094 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  94:	55                   	push   %ebp
  95:	89 e5                	mov    %esp,%ebp
  97:	57                   	push   %edi
  98:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  99:	8b 4d 08             	mov    0x8(%ebp),%ecx
  9c:	8b 55 10             	mov    0x10(%ebp),%edx
  9f:	8b 45 0c             	mov    0xc(%ebp),%eax
  a2:	89 cb                	mov    %ecx,%ebx
  a4:	89 df                	mov    %ebx,%edi
  a6:	89 d1                	mov    %edx,%ecx
  a8:	fc                   	cld    
  a9:	f3 aa                	rep stos %al,%es:(%edi)
  ab:	89 ca                	mov    %ecx,%edx
  ad:	89 fb                	mov    %edi,%ebx
  af:	89 5d 08             	mov    %ebx,0x8(%ebp)
  b2:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  b5:	5b                   	pop    %ebx
  b6:	5f                   	pop    %edi
  b7:	5d                   	pop    %ebp
  b8:	c3                   	ret    

000000b9 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  b9:	55                   	push   %ebp
  ba:	89 e5                	mov    %esp,%ebp
  bc:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  bf:	8b 45 08             	mov    0x8(%ebp),%eax
  c2:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  c5:	8b 45 0c             	mov    0xc(%ebp),%eax
  c8:	0f b6 10             	movzbl (%eax),%edx
  cb:	8b 45 08             	mov    0x8(%ebp),%eax
  ce:	88 10                	mov    %dl,(%eax)
  d0:	8b 45 08             	mov    0x8(%ebp),%eax
  d3:	0f b6 00             	movzbl (%eax),%eax
  d6:	84 c0                	test   %al,%al
  d8:	0f 95 c0             	setne  %al
  db:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  df:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
  e3:	84 c0                	test   %al,%al
  e5:	75 de                	jne    c5 <strcpy+0xc>
    ;
  return os;
  e7:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  ea:	c9                   	leave  
  eb:	c3                   	ret    

000000ec <strcmp>:

int
strcmp(const char *p, const char *q)
{
  ec:	55                   	push   %ebp
  ed:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
  ef:	eb 08                	jmp    f9 <strcmp+0xd>
    p++, q++;
  f1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  f5:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
  f9:	8b 45 08             	mov    0x8(%ebp),%eax
  fc:	0f b6 00             	movzbl (%eax),%eax
  ff:	84 c0                	test   %al,%al
 101:	74 10                	je     113 <strcmp+0x27>
 103:	8b 45 08             	mov    0x8(%ebp),%eax
 106:	0f b6 10             	movzbl (%eax),%edx
 109:	8b 45 0c             	mov    0xc(%ebp),%eax
 10c:	0f b6 00             	movzbl (%eax),%eax
 10f:	38 c2                	cmp    %al,%dl
 111:	74 de                	je     f1 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 113:	8b 45 08             	mov    0x8(%ebp),%eax
 116:	0f b6 00             	movzbl (%eax),%eax
 119:	0f b6 d0             	movzbl %al,%edx
 11c:	8b 45 0c             	mov    0xc(%ebp),%eax
 11f:	0f b6 00             	movzbl (%eax),%eax
 122:	0f b6 c0             	movzbl %al,%eax
 125:	89 d1                	mov    %edx,%ecx
 127:	29 c1                	sub    %eax,%ecx
 129:	89 c8                	mov    %ecx,%eax
}
 12b:	5d                   	pop    %ebp
 12c:	c3                   	ret    

0000012d <strlen>:

uint
strlen(char *s)
{
 12d:	55                   	push   %ebp
 12e:	89 e5                	mov    %esp,%ebp
 130:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 133:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 13a:	eb 04                	jmp    140 <strlen+0x13>
 13c:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 140:	8b 45 fc             	mov    -0x4(%ebp),%eax
 143:	03 45 08             	add    0x8(%ebp),%eax
 146:	0f b6 00             	movzbl (%eax),%eax
 149:	84 c0                	test   %al,%al
 14b:	75 ef                	jne    13c <strlen+0xf>
    ;
  return n;
 14d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 150:	c9                   	leave  
 151:	c3                   	ret    

00000152 <memset>:

void*
memset(void *dst, int c, uint n)
{
 152:	55                   	push   %ebp
 153:	89 e5                	mov    %esp,%ebp
 155:	83 ec 0c             	sub    $0xc,%esp
  stosb(dst, c, n);
 158:	8b 45 10             	mov    0x10(%ebp),%eax
 15b:	89 44 24 08          	mov    %eax,0x8(%esp)
 15f:	8b 45 0c             	mov    0xc(%ebp),%eax
 162:	89 44 24 04          	mov    %eax,0x4(%esp)
 166:	8b 45 08             	mov    0x8(%ebp),%eax
 169:	89 04 24             	mov    %eax,(%esp)
 16c:	e8 23 ff ff ff       	call   94 <stosb>
  return dst;
 171:	8b 45 08             	mov    0x8(%ebp),%eax
}
 174:	c9                   	leave  
 175:	c3                   	ret    

00000176 <strchr>:

char*
strchr(const char *s, char c)
{
 176:	55                   	push   %ebp
 177:	89 e5                	mov    %esp,%ebp
 179:	83 ec 04             	sub    $0x4,%esp
 17c:	8b 45 0c             	mov    0xc(%ebp),%eax
 17f:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 182:	eb 14                	jmp    198 <strchr+0x22>
    if(*s == c)
 184:	8b 45 08             	mov    0x8(%ebp),%eax
 187:	0f b6 00             	movzbl (%eax),%eax
 18a:	3a 45 fc             	cmp    -0x4(%ebp),%al
 18d:	75 05                	jne    194 <strchr+0x1e>
      return (char*)s;
 18f:	8b 45 08             	mov    0x8(%ebp),%eax
 192:	eb 13                	jmp    1a7 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 194:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 198:	8b 45 08             	mov    0x8(%ebp),%eax
 19b:	0f b6 00             	movzbl (%eax),%eax
 19e:	84 c0                	test   %al,%al
 1a0:	75 e2                	jne    184 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 1a2:	b8 00 00 00 00       	mov    $0x0,%eax
}
 1a7:	c9                   	leave  
 1a8:	c3                   	ret    

000001a9 <gets>:

char*
gets(char *buf, int max)
{
 1a9:	55                   	push   %ebp
 1aa:	89 e5                	mov    %esp,%ebp
 1ac:	83 ec 28             	sub    $0x28,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1af:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 1b6:	eb 44                	jmp    1fc <gets+0x53>
    cc = read(0, &c, 1);
 1b8:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 1bf:	00 
 1c0:	8d 45 ef             	lea    -0x11(%ebp),%eax
 1c3:	89 44 24 04          	mov    %eax,0x4(%esp)
 1c7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
 1ce:	e8 3d 01 00 00       	call   310 <read>
 1d3:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(cc < 1)
 1d6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1da:	7e 2d                	jle    209 <gets+0x60>
      break;
    buf[i++] = c;
 1dc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1df:	03 45 08             	add    0x8(%ebp),%eax
 1e2:	0f b6 55 ef          	movzbl -0x11(%ebp),%edx
 1e6:	88 10                	mov    %dl,(%eax)
 1e8:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
    if(c == '\n' || c == '\r')
 1ec:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1f0:	3c 0a                	cmp    $0xa,%al
 1f2:	74 16                	je     20a <gets+0x61>
 1f4:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1f8:	3c 0d                	cmp    $0xd,%al
 1fa:	74 0e                	je     20a <gets+0x61>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1fc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 1ff:	83 c0 01             	add    $0x1,%eax
 202:	3b 45 0c             	cmp    0xc(%ebp),%eax
 205:	7c b1                	jl     1b8 <gets+0xf>
 207:	eb 01                	jmp    20a <gets+0x61>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 209:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 20a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 20d:	03 45 08             	add    0x8(%ebp),%eax
 210:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 213:	8b 45 08             	mov    0x8(%ebp),%eax
}
 216:	c9                   	leave  
 217:	c3                   	ret    

00000218 <stat>:

int
stat(char *n, struct stat *st)
{
 218:	55                   	push   %ebp
 219:	89 e5                	mov    %esp,%ebp
 21b:	83 ec 28             	sub    $0x28,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 21e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
 225:	00 
 226:	8b 45 08             	mov    0x8(%ebp),%eax
 229:	89 04 24             	mov    %eax,(%esp)
 22c:	e8 07 01 00 00       	call   338 <open>
 231:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(fd < 0)
 234:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 238:	79 07                	jns    241 <stat+0x29>
    return -1;
 23a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 23f:	eb 23                	jmp    264 <stat+0x4c>
  r = fstat(fd, st);
 241:	8b 45 0c             	mov    0xc(%ebp),%eax
 244:	89 44 24 04          	mov    %eax,0x4(%esp)
 248:	8b 45 f0             	mov    -0x10(%ebp),%eax
 24b:	89 04 24             	mov    %eax,(%esp)
 24e:	e8 fd 00 00 00       	call   350 <fstat>
 253:	89 45 f4             	mov    %eax,-0xc(%ebp)
  close(fd);
 256:	8b 45 f0             	mov    -0x10(%ebp),%eax
 259:	89 04 24             	mov    %eax,(%esp)
 25c:	e8 bf 00 00 00       	call   320 <close>
  return r;
 261:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
 264:	c9                   	leave  
 265:	c3                   	ret    

00000266 <atoi>:

int
atoi(const char *s)
{
 266:	55                   	push   %ebp
 267:	89 e5                	mov    %esp,%ebp
 269:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 26c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 273:	eb 24                	jmp    299 <atoi+0x33>
    n = n*10 + *s++ - '0';
 275:	8b 55 fc             	mov    -0x4(%ebp),%edx
 278:	89 d0                	mov    %edx,%eax
 27a:	c1 e0 02             	shl    $0x2,%eax
 27d:	01 d0                	add    %edx,%eax
 27f:	01 c0                	add    %eax,%eax
 281:	89 c2                	mov    %eax,%edx
 283:	8b 45 08             	mov    0x8(%ebp),%eax
 286:	0f b6 00             	movzbl (%eax),%eax
 289:	0f be c0             	movsbl %al,%eax
 28c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 28f:	83 e8 30             	sub    $0x30,%eax
 292:	89 45 fc             	mov    %eax,-0x4(%ebp)
 295:	83 45 08 01          	addl   $0x1,0x8(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 299:	8b 45 08             	mov    0x8(%ebp),%eax
 29c:	0f b6 00             	movzbl (%eax),%eax
 29f:	3c 2f                	cmp    $0x2f,%al
 2a1:	7e 0a                	jle    2ad <atoi+0x47>
 2a3:	8b 45 08             	mov    0x8(%ebp),%eax
 2a6:	0f b6 00             	movzbl (%eax),%eax
 2a9:	3c 39                	cmp    $0x39,%al
 2ab:	7e c8                	jle    275 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 2ad:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2b0:	c9                   	leave  
 2b1:	c3                   	ret    

000002b2 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 2b2:	55                   	push   %ebp
 2b3:	89 e5                	mov    %esp,%ebp
 2b5:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 2b8:	8b 45 08             	mov    0x8(%ebp),%eax
 2bb:	89 45 f8             	mov    %eax,-0x8(%ebp)
  src = vsrc;
 2be:	8b 45 0c             	mov    0xc(%ebp),%eax
 2c1:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0)
 2c4:	eb 13                	jmp    2d9 <memmove+0x27>
    *dst++ = *src++;
 2c6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2c9:	0f b6 10             	movzbl (%eax),%edx
 2cc:	8b 45 f8             	mov    -0x8(%ebp),%eax
 2cf:	88 10                	mov    %dl,(%eax)
 2d1:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
 2d5:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2d9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
 2dd:	0f 9f c0             	setg   %al
 2e0:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
 2e4:	84 c0                	test   %al,%al
 2e6:	75 de                	jne    2c6 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 2e8:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2eb:	c9                   	leave  
 2ec:	c3                   	ret    
 2ed:	90                   	nop
 2ee:	90                   	nop
 2ef:	90                   	nop

000002f0 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 2f0:	b8 01 00 00 00       	mov    $0x1,%eax
 2f5:	cd 40                	int    $0x40
 2f7:	c3                   	ret    

000002f8 <exit>:
SYSCALL(exit)
 2f8:	b8 02 00 00 00       	mov    $0x2,%eax
 2fd:	cd 40                	int    $0x40
 2ff:	c3                   	ret    

00000300 <wait>:
SYSCALL(wait)
 300:	b8 03 00 00 00       	mov    $0x3,%eax
 305:	cd 40                	int    $0x40
 307:	c3                   	ret    

00000308 <pipe>:
SYSCALL(pipe)
 308:	b8 04 00 00 00       	mov    $0x4,%eax
 30d:	cd 40                	int    $0x40
 30f:	c3                   	ret    

00000310 <read>:
SYSCALL(read)
 310:	b8 05 00 00 00       	mov    $0x5,%eax
 315:	cd 40                	int    $0x40
 317:	c3                   	ret    

00000318 <write>:
SYSCALL(write)
 318:	b8 10 00 00 00       	mov    $0x10,%eax
 31d:	cd 40                	int    $0x40
 31f:	c3                   	ret    

00000320 <close>:
SYSCALL(close)
 320:	b8 15 00 00 00       	mov    $0x15,%eax
 325:	cd 40                	int    $0x40
 327:	c3                   	ret    

00000328 <kill>:
SYSCALL(kill)
 328:	b8 06 00 00 00       	mov    $0x6,%eax
 32d:	cd 40                	int    $0x40
 32f:	c3                   	ret    

00000330 <exec>:
SYSCALL(exec)
 330:	b8 07 00 00 00       	mov    $0x7,%eax
 335:	cd 40                	int    $0x40
 337:	c3                   	ret    

00000338 <open>:
SYSCALL(open)
 338:	b8 0f 00 00 00       	mov    $0xf,%eax
 33d:	cd 40                	int    $0x40
 33f:	c3                   	ret    

00000340 <mknod>:
SYSCALL(mknod)
 340:	b8 11 00 00 00       	mov    $0x11,%eax
 345:	cd 40                	int    $0x40
 347:	c3                   	ret    

00000348 <unlink>:
SYSCALL(unlink)
 348:	b8 12 00 00 00       	mov    $0x12,%eax
 34d:	cd 40                	int    $0x40
 34f:	c3                   	ret    

00000350 <fstat>:
SYSCALL(fstat)
 350:	b8 08 00 00 00       	mov    $0x8,%eax
 355:	cd 40                	int    $0x40
 357:	c3                   	ret    

00000358 <link>:
SYSCALL(link)
 358:	b8 13 00 00 00       	mov    $0x13,%eax
 35d:	cd 40                	int    $0x40
 35f:	c3                   	ret    

00000360 <mkdir>:
SYSCALL(mkdir)
 360:	b8 14 00 00 00       	mov    $0x14,%eax
 365:	cd 40                	int    $0x40
 367:	c3                   	ret    

00000368 <chdir>:
SYSCALL(chdir)
 368:	b8 09 00 00 00       	mov    $0x9,%eax
 36d:	cd 40                	int    $0x40
 36f:	c3                   	ret    

00000370 <dup>:
SYSCALL(dup)
 370:	b8 0a 00 00 00       	mov    $0xa,%eax
 375:	cd 40                	int    $0x40
 377:	c3                   	ret    

00000378 <getpid>:
SYSCALL(getpid)
 378:	b8 0b 00 00 00       	mov    $0xb,%eax
 37d:	cd 40                	int    $0x40
 37f:	c3                   	ret    

00000380 <sbrk>:
SYSCALL(sbrk)
 380:	b8 0c 00 00 00       	mov    $0xc,%eax
 385:	cd 40                	int    $0x40
 387:	c3                   	ret    

00000388 <sleep>:
SYSCALL(sleep)
 388:	b8 0d 00 00 00       	mov    $0xd,%eax
 38d:	cd 40                	int    $0x40
 38f:	c3                   	ret    

00000390 <uptime>:
SYSCALL(uptime)
 390:	b8 0e 00 00 00       	mov    $0xe,%eax
 395:	cd 40                	int    $0x40
 397:	c3                   	ret    

00000398 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 398:	55                   	push   %ebp
 399:	89 e5                	mov    %esp,%ebp
 39b:	83 ec 28             	sub    $0x28,%esp
 39e:	8b 45 0c             	mov    0xc(%ebp),%eax
 3a1:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 3a4:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
 3ab:	00 
 3ac:	8d 45 f4             	lea    -0xc(%ebp),%eax
 3af:	89 44 24 04          	mov    %eax,0x4(%esp)
 3b3:	8b 45 08             	mov    0x8(%ebp),%eax
 3b6:	89 04 24             	mov    %eax,(%esp)
 3b9:	e8 5a ff ff ff       	call   318 <write>
}
 3be:	c9                   	leave  
 3bf:	c3                   	ret    

000003c0 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 3c0:	55                   	push   %ebp
 3c1:	89 e5                	mov    %esp,%ebp
 3c3:	53                   	push   %ebx
 3c4:	83 ec 44             	sub    $0x44,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 3c7:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 3ce:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 3d2:	74 17                	je     3eb <printint+0x2b>
 3d4:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 3d8:	79 11                	jns    3eb <printint+0x2b>
    neg = 1;
 3da:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 3e1:	8b 45 0c             	mov    0xc(%ebp),%eax
 3e4:	f7 d8                	neg    %eax
 3e6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 3e9:	eb 06                	jmp    3f1 <printint+0x31>
    neg = 1;
    x = -xx;
  } else {
    x = xx;
 3eb:	8b 45 0c             	mov    0xc(%ebp),%eax
 3ee:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  i = 0;
 3f1:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  do{
    buf[i++] = digits[x % base];
 3f8:	8b 4d ec             	mov    -0x14(%ebp),%ecx
 3fb:	8b 5d 10             	mov    0x10(%ebp),%ebx
 3fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
 401:	ba 00 00 00 00       	mov    $0x0,%edx
 406:	f7 f3                	div    %ebx
 408:	89 d0                	mov    %edx,%eax
 40a:	0f b6 80 64 08 00 00 	movzbl 0x864(%eax),%eax
 411:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
 415:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  }while((x /= base) != 0);
 419:	8b 45 10             	mov    0x10(%ebp),%eax
 41c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 41f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 422:	ba 00 00 00 00       	mov    $0x0,%edx
 427:	f7 75 d4             	divl   -0x2c(%ebp)
 42a:	89 45 f4             	mov    %eax,-0xc(%ebp)
 42d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 431:	75 c5                	jne    3f8 <printint+0x38>
  if(neg)
 433:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 437:	74 2a                	je     463 <printint+0xa3>
    buf[i++] = '-';
 439:	8b 45 ec             	mov    -0x14(%ebp),%eax
 43c:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)
 441:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)

  while(--i >= 0)
 445:	eb 1d                	jmp    464 <printint+0xa4>
    putc(fd, buf[i]);
 447:	8b 45 ec             	mov    -0x14(%ebp),%eax
 44a:	0f b6 44 05 dc       	movzbl -0x24(%ebp,%eax,1),%eax
 44f:	0f be c0             	movsbl %al,%eax
 452:	89 44 24 04          	mov    %eax,0x4(%esp)
 456:	8b 45 08             	mov    0x8(%ebp),%eax
 459:	89 04 24             	mov    %eax,(%esp)
 45c:	e8 37 ff ff ff       	call   398 <putc>
 461:	eb 01                	jmp    464 <printint+0xa4>
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 463:	90                   	nop
 464:	83 6d ec 01          	subl   $0x1,-0x14(%ebp)
 468:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 46c:	79 d9                	jns    447 <printint+0x87>
    putc(fd, buf[i]);
}
 46e:	83 c4 44             	add    $0x44,%esp
 471:	5b                   	pop    %ebx
 472:	5d                   	pop    %ebp
 473:	c3                   	ret    

00000474 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 474:	55                   	push   %ebp
 475:	89 e5                	mov    %esp,%ebp
 477:	83 ec 38             	sub    $0x38,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 47a:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 481:	8d 45 0c             	lea    0xc(%ebp),%eax
 484:	83 c0 04             	add    $0x4,%eax
 487:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(i = 0; fmt[i]; i++){
 48a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
 491:	e9 7e 01 00 00       	jmp    614 <printf+0x1a0>
    c = fmt[i] & 0xff;
 496:	8b 55 0c             	mov    0xc(%ebp),%edx
 499:	8b 45 ec             	mov    -0x14(%ebp),%eax
 49c:	8d 04 02             	lea    (%edx,%eax,1),%eax
 49f:	0f b6 00             	movzbl (%eax),%eax
 4a2:	0f be c0             	movsbl %al,%eax
 4a5:	25 ff 00 00 00       	and    $0xff,%eax
 4aa:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(state == 0){
 4ad:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4b1:	75 2c                	jne    4df <printf+0x6b>
      if(c == '%'){
 4b3:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 4b7:	75 0c                	jne    4c5 <printf+0x51>
        state = '%';
 4b9:	c7 45 f0 25 00 00 00 	movl   $0x25,-0x10(%ebp)
 4c0:	e9 4b 01 00 00       	jmp    610 <printf+0x19c>
      } else {
        putc(fd, c);
 4c5:	8b 45 e8             	mov    -0x18(%ebp),%eax
 4c8:	0f be c0             	movsbl %al,%eax
 4cb:	89 44 24 04          	mov    %eax,0x4(%esp)
 4cf:	8b 45 08             	mov    0x8(%ebp),%eax
 4d2:	89 04 24             	mov    %eax,(%esp)
 4d5:	e8 be fe ff ff       	call   398 <putc>
 4da:	e9 31 01 00 00       	jmp    610 <printf+0x19c>
      }
    } else if(state == '%'){
 4df:	83 7d f0 25          	cmpl   $0x25,-0x10(%ebp)
 4e3:	0f 85 27 01 00 00    	jne    610 <printf+0x19c>
      if(c == 'd'){
 4e9:	83 7d e8 64          	cmpl   $0x64,-0x18(%ebp)
 4ed:	75 2d                	jne    51c <printf+0xa8>
        printint(fd, *ap, 10, 1);
 4ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4f2:	8b 00                	mov    (%eax),%eax
 4f4:	c7 44 24 0c 01 00 00 	movl   $0x1,0xc(%esp)
 4fb:	00 
 4fc:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 503:	00 
 504:	89 44 24 04          	mov    %eax,0x4(%esp)
 508:	8b 45 08             	mov    0x8(%ebp),%eax
 50b:	89 04 24             	mov    %eax,(%esp)
 50e:	e8 ad fe ff ff       	call   3c0 <printint>
        ap++;
 513:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 517:	e9 ed 00 00 00       	jmp    609 <printf+0x195>
      } else if(c == 'x' || c == 'p'){
 51c:	83 7d e8 78          	cmpl   $0x78,-0x18(%ebp)
 520:	74 06                	je     528 <printf+0xb4>
 522:	83 7d e8 70          	cmpl   $0x70,-0x18(%ebp)
 526:	75 2d                	jne    555 <printf+0xe1>
        printint(fd, *ap, 16, 0);
 528:	8b 45 f4             	mov    -0xc(%ebp),%eax
 52b:	8b 00                	mov    (%eax),%eax
 52d:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
 534:	00 
 535:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
 53c:	00 
 53d:	89 44 24 04          	mov    %eax,0x4(%esp)
 541:	8b 45 08             	mov    0x8(%ebp),%eax
 544:	89 04 24             	mov    %eax,(%esp)
 547:	e8 74 fe ff ff       	call   3c0 <printint>
        ap++;
 54c:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
      }
    } else if(state == '%'){
      if(c == 'd'){
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 550:	e9 b4 00 00 00       	jmp    609 <printf+0x195>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 555:	83 7d e8 73          	cmpl   $0x73,-0x18(%ebp)
 559:	75 46                	jne    5a1 <printf+0x12d>
        s = (char*)*ap;
 55b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 55e:	8b 00                	mov    (%eax),%eax
 560:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        ap++;
 563:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
        if(s == 0)
 567:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
 56b:	75 27                	jne    594 <printf+0x120>
          s = "(null)";
 56d:	c7 45 e4 5d 08 00 00 	movl   $0x85d,-0x1c(%ebp)
        while(*s != 0){
 574:	eb 1f                	jmp    595 <printf+0x121>
          putc(fd, *s);
 576:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 579:	0f b6 00             	movzbl (%eax),%eax
 57c:	0f be c0             	movsbl %al,%eax
 57f:	89 44 24 04          	mov    %eax,0x4(%esp)
 583:	8b 45 08             	mov    0x8(%ebp),%eax
 586:	89 04 24             	mov    %eax,(%esp)
 589:	e8 0a fe ff ff       	call   398 <putc>
          s++;
 58e:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 592:	eb 01                	jmp    595 <printf+0x121>
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 594:	90                   	nop
 595:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 598:	0f b6 00             	movzbl (%eax),%eax
 59b:	84 c0                	test   %al,%al
 59d:	75 d7                	jne    576 <printf+0x102>
 59f:	eb 68                	jmp    609 <printf+0x195>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 5a1:	83 7d e8 63          	cmpl   $0x63,-0x18(%ebp)
 5a5:	75 1d                	jne    5c4 <printf+0x150>
        putc(fd, *ap);
 5a7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5aa:	8b 00                	mov    (%eax),%eax
 5ac:	0f be c0             	movsbl %al,%eax
 5af:	89 44 24 04          	mov    %eax,0x4(%esp)
 5b3:	8b 45 08             	mov    0x8(%ebp),%eax
 5b6:	89 04 24             	mov    %eax,(%esp)
 5b9:	e8 da fd ff ff       	call   398 <putc>
        ap++;
 5be:	83 45 f4 04          	addl   $0x4,-0xc(%ebp)
 5c2:	eb 45                	jmp    609 <printf+0x195>
      } else if(c == '%'){
 5c4:	83 7d e8 25          	cmpl   $0x25,-0x18(%ebp)
 5c8:	75 17                	jne    5e1 <printf+0x16d>
        putc(fd, c);
 5ca:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5cd:	0f be c0             	movsbl %al,%eax
 5d0:	89 44 24 04          	mov    %eax,0x4(%esp)
 5d4:	8b 45 08             	mov    0x8(%ebp),%eax
 5d7:	89 04 24             	mov    %eax,(%esp)
 5da:	e8 b9 fd ff ff       	call   398 <putc>
 5df:	eb 28                	jmp    609 <printf+0x195>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 5e1:	c7 44 24 04 25 00 00 	movl   $0x25,0x4(%esp)
 5e8:	00 
 5e9:	8b 45 08             	mov    0x8(%ebp),%eax
 5ec:	89 04 24             	mov    %eax,(%esp)
 5ef:	e8 a4 fd ff ff       	call   398 <putc>
        putc(fd, c);
 5f4:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5f7:	0f be c0             	movsbl %al,%eax
 5fa:	89 44 24 04          	mov    %eax,0x4(%esp)
 5fe:	8b 45 08             	mov    0x8(%ebp),%eax
 601:	89 04 24             	mov    %eax,(%esp)
 604:	e8 8f fd ff ff       	call   398 <putc>
      }
      state = 0;
 609:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 610:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
 614:	8b 55 0c             	mov    0xc(%ebp),%edx
 617:	8b 45 ec             	mov    -0x14(%ebp),%eax
 61a:	8d 04 02             	lea    (%edx,%eax,1),%eax
 61d:	0f b6 00             	movzbl (%eax),%eax
 620:	84 c0                	test   %al,%al
 622:	0f 85 6e fe ff ff    	jne    496 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 628:	c9                   	leave  
 629:	c3                   	ret    
 62a:	90                   	nop
 62b:	90                   	nop

0000062c <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 62c:	55                   	push   %ebp
 62d:	89 e5                	mov    %esp,%ebp
 62f:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 632:	8b 45 08             	mov    0x8(%ebp),%eax
 635:	83 e8 08             	sub    $0x8,%eax
 638:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 63b:	a1 88 08 00 00       	mov    0x888,%eax
 640:	89 45 fc             	mov    %eax,-0x4(%ebp)
 643:	eb 24                	jmp    669 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 645:	8b 45 fc             	mov    -0x4(%ebp),%eax
 648:	8b 00                	mov    (%eax),%eax
 64a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 64d:	77 12                	ja     661 <free+0x35>
 64f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 652:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 655:	77 24                	ja     67b <free+0x4f>
 657:	8b 45 fc             	mov    -0x4(%ebp),%eax
 65a:	8b 00                	mov    (%eax),%eax
 65c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 65f:	77 1a                	ja     67b <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 661:	8b 45 fc             	mov    -0x4(%ebp),%eax
 664:	8b 00                	mov    (%eax),%eax
 666:	89 45 fc             	mov    %eax,-0x4(%ebp)
 669:	8b 45 f8             	mov    -0x8(%ebp),%eax
 66c:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 66f:	76 d4                	jbe    645 <free+0x19>
 671:	8b 45 fc             	mov    -0x4(%ebp),%eax
 674:	8b 00                	mov    (%eax),%eax
 676:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 679:	76 ca                	jbe    645 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 67b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 67e:	8b 40 04             	mov    0x4(%eax),%eax
 681:	c1 e0 03             	shl    $0x3,%eax
 684:	89 c2                	mov    %eax,%edx
 686:	03 55 f8             	add    -0x8(%ebp),%edx
 689:	8b 45 fc             	mov    -0x4(%ebp),%eax
 68c:	8b 00                	mov    (%eax),%eax
 68e:	39 c2                	cmp    %eax,%edx
 690:	75 24                	jne    6b6 <free+0x8a>
    bp->s.size += p->s.ptr->s.size;
 692:	8b 45 f8             	mov    -0x8(%ebp),%eax
 695:	8b 50 04             	mov    0x4(%eax),%edx
 698:	8b 45 fc             	mov    -0x4(%ebp),%eax
 69b:	8b 00                	mov    (%eax),%eax
 69d:	8b 40 04             	mov    0x4(%eax),%eax
 6a0:	01 c2                	add    %eax,%edx
 6a2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6a5:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 6a8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ab:	8b 00                	mov    (%eax),%eax
 6ad:	8b 10                	mov    (%eax),%edx
 6af:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6b2:	89 10                	mov    %edx,(%eax)
 6b4:	eb 0a                	jmp    6c0 <free+0x94>
  } else
    bp->s.ptr = p->s.ptr;
 6b6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6b9:	8b 10                	mov    (%eax),%edx
 6bb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6be:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 6c0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6c3:	8b 40 04             	mov    0x4(%eax),%eax
 6c6:	c1 e0 03             	shl    $0x3,%eax
 6c9:	03 45 fc             	add    -0x4(%ebp),%eax
 6cc:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6cf:	75 20                	jne    6f1 <free+0xc5>
    p->s.size += bp->s.size;
 6d1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d4:	8b 50 04             	mov    0x4(%eax),%edx
 6d7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6da:	8b 40 04             	mov    0x4(%eax),%eax
 6dd:	01 c2                	add    %eax,%edx
 6df:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e2:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 6e5:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6e8:	8b 10                	mov    (%eax),%edx
 6ea:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ed:	89 10                	mov    %edx,(%eax)
 6ef:	eb 08                	jmp    6f9 <free+0xcd>
  } else
    p->s.ptr = bp;
 6f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6f4:	8b 55 f8             	mov    -0x8(%ebp),%edx
 6f7:	89 10                	mov    %edx,(%eax)
  freep = p;
 6f9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6fc:	a3 88 08 00 00       	mov    %eax,0x888
}
 701:	c9                   	leave  
 702:	c3                   	ret    

00000703 <morecore>:

static Header*
morecore(uint nu)
{
 703:	55                   	push   %ebp
 704:	89 e5                	mov    %esp,%ebp
 706:	83 ec 28             	sub    $0x28,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 709:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 710:	77 07                	ja     719 <morecore+0x16>
    nu = 4096;
 712:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 719:	8b 45 08             	mov    0x8(%ebp),%eax
 71c:	c1 e0 03             	shl    $0x3,%eax
 71f:	89 04 24             	mov    %eax,(%esp)
 722:	e8 59 fc ff ff       	call   380 <sbrk>
 727:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(p == (char*)-1)
 72a:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
 72e:	75 07                	jne    737 <morecore+0x34>
    return 0;
 730:	b8 00 00 00 00       	mov    $0x0,%eax
 735:	eb 22                	jmp    759 <morecore+0x56>
  hp = (Header*)p;
 737:	8b 45 f0             	mov    -0x10(%ebp),%eax
 73a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  hp->s.size = nu;
 73d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 740:	8b 55 08             	mov    0x8(%ebp),%edx
 743:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 746:	8b 45 f4             	mov    -0xc(%ebp),%eax
 749:	83 c0 08             	add    $0x8,%eax
 74c:	89 04 24             	mov    %eax,(%esp)
 74f:	e8 d8 fe ff ff       	call   62c <free>
  return freep;
 754:	a1 88 08 00 00       	mov    0x888,%eax
}
 759:	c9                   	leave  
 75a:	c3                   	ret    

0000075b <malloc>:

void*
malloc(uint nbytes)
{
 75b:	55                   	push   %ebp
 75c:	89 e5                	mov    %esp,%ebp
 75e:	83 ec 28             	sub    $0x28,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 761:	8b 45 08             	mov    0x8(%ebp),%eax
 764:	83 c0 07             	add    $0x7,%eax
 767:	c1 e8 03             	shr    $0x3,%eax
 76a:	83 c0 01             	add    $0x1,%eax
 76d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((prevp = freep) == 0){
 770:	a1 88 08 00 00       	mov    0x888,%eax
 775:	89 45 f0             	mov    %eax,-0x10(%ebp)
 778:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 77c:	75 23                	jne    7a1 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 77e:	c7 45 f0 80 08 00 00 	movl   $0x880,-0x10(%ebp)
 785:	8b 45 f0             	mov    -0x10(%ebp),%eax
 788:	a3 88 08 00 00       	mov    %eax,0x888
 78d:	a1 88 08 00 00       	mov    0x888,%eax
 792:	a3 80 08 00 00       	mov    %eax,0x880
    base.s.size = 0;
 797:	c7 05 84 08 00 00 00 	movl   $0x0,0x884
 79e:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7a1:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7a4:	8b 00                	mov    (%eax),%eax
 7a6:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p->s.size >= nunits){
 7a9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7ac:	8b 40 04             	mov    0x4(%eax),%eax
 7af:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 7b2:	72 4d                	jb     801 <malloc+0xa6>
      if(p->s.size == nunits)
 7b4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7b7:	8b 40 04             	mov    0x4(%eax),%eax
 7ba:	3b 45 f4             	cmp    -0xc(%ebp),%eax
 7bd:	75 0c                	jne    7cb <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 7bf:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7c2:	8b 10                	mov    (%eax),%edx
 7c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7c7:	89 10                	mov    %edx,(%eax)
 7c9:	eb 26                	jmp    7f1 <malloc+0x96>
      else {
        p->s.size -= nunits;
 7cb:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7ce:	8b 40 04             	mov    0x4(%eax),%eax
 7d1:	89 c2                	mov    %eax,%edx
 7d3:	2b 55 f4             	sub    -0xc(%ebp),%edx
 7d6:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7d9:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 7dc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7df:	8b 40 04             	mov    0x4(%eax),%eax
 7e2:	c1 e0 03             	shl    $0x3,%eax
 7e5:	01 45 ec             	add    %eax,-0x14(%ebp)
        p->s.size = nunits;
 7e8:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7eb:	8b 55 f4             	mov    -0xc(%ebp),%edx
 7ee:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 7f1:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7f4:	a3 88 08 00 00       	mov    %eax,0x888
      return (void*)(p + 1);
 7f9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7fc:	83 c0 08             	add    $0x8,%eax
 7ff:	eb 38                	jmp    839 <malloc+0xde>
    }
    if(p == freep)
 801:	a1 88 08 00 00       	mov    0x888,%eax
 806:	39 45 ec             	cmp    %eax,-0x14(%ebp)
 809:	75 1b                	jne    826 <malloc+0xcb>
      if((p = morecore(nunits)) == 0)
 80b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 80e:	89 04 24             	mov    %eax,(%esp)
 811:	e8 ed fe ff ff       	call   703 <morecore>
 816:	89 45 ec             	mov    %eax,-0x14(%ebp)
 819:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 81d:	75 07                	jne    826 <malloc+0xcb>
        return 0;
 81f:	b8 00 00 00 00       	mov    $0x0,%eax
 824:	eb 13                	jmp    839 <malloc+0xde>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 826:	8b 45 ec             	mov    -0x14(%ebp),%eax
 829:	89 45 f0             	mov    %eax,-0x10(%ebp)
 82c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 82f:	8b 00                	mov    (%eax),%eax
 831:	89 45 ec             	mov    %eax,-0x14(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 834:	e9 70 ff ff ff       	jmp    7a9 <malloc+0x4e>
}
 839:	c9                   	leave  
 83a:	c3                   	ret    
