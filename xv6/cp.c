#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

char buf[512];

void cp(char* filename, char* filenamecopy) {
  int f, fd, readFile, writeFile;
 
  if((f = open(filename, O_RDONLY)) < 0) {
    printf(1, "cp: Error, cannot open %s\n", filename);
    exit();
  }

  if((fd = open(filenamecopy, O_CREATE|O_WRONLY)) < 0) {
    printf(1, "cp: Error, cannot open %s\n", filenamecopy);
    exit();
  }

  while((readFile = read(f, buf, sizeof(buf))) > 0) {
    writeFile = write(fd, buf, readFile);
  }
    close(f);
    close(fd);
}

int
main(int argc, char *argv[]) {

//  int fn, i; //fnc, i;

  if(argc <= 1){
    exit();
  }

//  for(i = 1; i < argc; i++){
//    if((fn = open(argv[i], 0)) < 0){
//      printf(1, "cp: Error, cannot open %s\n", argv[i]);
//      exit();
 //   }
    cp(argv[1], argv[2]);
//  }
  exit();
}
