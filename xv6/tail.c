#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

char buf[512];

void
tail(char* filename)
{
  int f, readFile, counter;
  char *newLine, *bufCP;

  if((f = open(filename, 0)) < 0){
    printf(1, "tail: cannot open %s\n", filename);
    exit();
  }

  counter = 0;
  while((readFile = read(f, buf, sizeof(buf))) > 0){
    bufCP = buf;
    while((newLine = strchr(bufCP, '\n')) != 0){
      *newLine = 0;
      bufCP = newLine+1;
      counter++;
    }
  }
  close(f);

  if((f = open(filename, 0)) < 0){
    printf(1, "tail: cannot open %s\n", filename);
    exit();
  }
  
  if(counter <= 10)
  {
    while((readFile = read(f, buf, sizeof(buf))) > 0)
      write(1, buf, readFile);
  }
  
  while((readFile = read(f, buf, sizeof(buf))) > 0){
    bufCP = buf;
    while((newLine = strchr(bufCP, '\n')) != 0){     
      counter--;
      if(counter < 10)
      {
        *newLine = '\n';
        write(1, bufCP, newLine+1 - bufCP);
      }
      *newLine = 0; 
      bufCP = newLine+1;
    }
  }
}

int
main(int argc, char *argv[])
{ 
  if(argc != 2){
    printf(1, "Error, cannot open &s\n", argv[1]);
    exit();
  }
  
  tail(argv[1]);
  exit();
}
