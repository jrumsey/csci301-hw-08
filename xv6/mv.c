#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

char buf[512];

void
mv(char* filename, char* filenamenew)
{
  int f, fn;
  if((f = link(filename, filenamenew) < 0) || (fn = unlink(filename) < 0)) {
    printf(1, "mv: Error, cannot move %s to %s\n", filename, filenamenew);
  }
//  close(f);
//  close(fn);
}

int
main(int argc, char *argv[])
{
  //int fd, i;

  if(argc <= 1){ 
    exit();
  }

    mv(argv[1], argv[2]);
  
  exit();
}
