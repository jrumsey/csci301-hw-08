#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

char buf[512];

void
head(char* filename)
{
  int f, count, readFile;
  char *newLine, *bufCP;
  
  if((f = open(filename, O_RDONLY)) < 0) {
    printf(1, "head: Error, cannot open %s\n", filename);
    exit();
  }

  count = 0;
  while((readFile = read(f, buf, sizeof(buf))) > 0){
    bufCP = buf;
    while((newLine = strchr(bufCP, '\n')) != 0){
      *newLine = '\n';
      write(1, bufCP, newLine-bufCP+1);
      *newLine = 0;
      bufCP = newLine + 1;
      count++;
      if(count > 9)
        return;
    }
  }
  close(f);
}

int
main(int argc, char *argv[])
{
    
  if(argc != 2){
    printf(1, "Error, cannot open %s\n", argv[1]);
    exit();
  }

  head(argv[1]);
  exit();
}
